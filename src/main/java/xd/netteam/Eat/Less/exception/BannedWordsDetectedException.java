package xd.netteam.Eat.Less.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BannedWordsDetectedException extends RuntimeException {
    public BannedWordsDetectedException() {
        super("Please remove all banned words from the input form.");
    }

    public BannedWordsDetectedException(String message) {
        super(message);
    }
}
