package xd.netteam.Eat.Less.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    Boolean existsByUsername(String username);
}
