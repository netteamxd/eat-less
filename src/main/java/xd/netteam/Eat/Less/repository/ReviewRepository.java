package xd.netteam.Eat.Less.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.Review;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long>{
}
