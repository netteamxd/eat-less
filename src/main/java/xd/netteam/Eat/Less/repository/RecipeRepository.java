package xd.netteam.Eat.Less.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.Recipe;

import java.util.List;

@Repository
public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long>, JpaSpecificationExecutor<Recipe>{

    @Query("SELECT DISTINCT r FROM Recipe r JOIN r.ingredients ri WHERE ri.ingredient NOT IN (\n" +
            "        SELECT ei.id FROM User u JOIN u.excludedIngredients ei WHERE u.id = :id\n" +
            ")")
    List<Recipe> findAllRecipesRecommendedForUserWithId(@Param("id") Long id);

    List<Recipe> findTop50ByOrderByAverageRating();
}
