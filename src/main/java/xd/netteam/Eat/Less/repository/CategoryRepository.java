package xd.netteam.Eat.Less.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepository  extends CrudRepository<Category, Long> {
    List<Category> findCategoryByChildCategoriesEmpty();
}
