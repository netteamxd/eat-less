package xd.netteam.Eat.Less.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.Ingredient;


@Repository
public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {
    Ingredient findByName(String name);
}
