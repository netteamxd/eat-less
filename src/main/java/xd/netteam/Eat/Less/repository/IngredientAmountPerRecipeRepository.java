package xd.netteam.Eat.Less.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xd.netteam.Eat.Less.model.entity.IngredientAmountPerRecipe;

@Repository
public interface IngredientAmountPerRecipeRepository extends CrudRepository<IngredientAmountPerRecipe, Long> {
}
