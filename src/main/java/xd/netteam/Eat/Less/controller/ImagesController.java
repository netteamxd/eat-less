package xd.netteam.Eat.Less.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xd.netteam.Eat.Less.service.StorageService;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/images")
public class ImagesController {
    private StorageService storageService;

    @Autowired
    public ImagesController(StorageService storageService) {
        this.storageService = storageService;
    }

    @ResponseBody
    @RequestMapping(path = "/{image:.+}", method = RequestMethod.GET, produces = "image/*")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Resource> serveFile(@PathVariable String image) {
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(600, TimeUnit.SECONDS).cachePublic())
                .body(storageService.load(image));
    }


    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public String handleFileUpload(@RequestParam("image") MultipartFile image) {
        return storageService.store(image);
    }
}
