package xd.netteam.Eat.Less.controller;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import xd.netteam.Eat.Less.model.DietInfo;
import xd.netteam.Eat.Less.model.converter.DietDtoConverter;
import xd.netteam.Eat.Less.model.dto.diet.DietDto;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.service.DietService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/diet")
public class DietController {
    private DietService dietService;
    private DietDtoConverter dietDtoConverter;

    public DietController(DietService dietService, DietDtoConverter dietDtoConverter) {
        this.dietService = dietService;
        this.dietDtoConverter = dietDtoConverter;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public DietDto generate(@Valid @RequestBody DietInfo dietInfo, @AuthenticationPrincipal User user) {
        return dietDtoConverter.convert(dietService.generateDiet(dietInfo, user));
    }
}
