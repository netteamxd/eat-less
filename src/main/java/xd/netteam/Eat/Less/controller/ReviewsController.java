package xd.netteam.Eat.Less.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import xd.netteam.Eat.Less.model.converter.ReviewDtoConverter;
import xd.netteam.Eat.Less.model.dto.review.ReviewCreationDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewDetailsDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewListDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewUpdateDto;
import xd.netteam.Eat.Less.model.entity.Review;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.service.ReviewService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/reviews")
public class ReviewsController {
    private ReviewService reviewService;

    private ReviewDtoConverter reviewDtoConverter;

    @Autowired
    public ReviewsController(ReviewService reviewService, ReviewDtoConverter reviewDtoConverter) {
        this.reviewService = reviewService;
        this.reviewDtoConverter = reviewDtoConverter;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<ReviewListDto> getAll() {
        return reviewService.getAll().stream()
                .map(reviewDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ReviewDetailsDto create(@Valid @RequestBody ReviewCreationDto review, @AuthenticationPrincipal User user) {
        Review reviewToCreate = reviewDtoConverter.fromCreation(review);
        reviewToCreate.setAuthor(user);
        Review createdReview = reviewService.create(reviewToCreate);
        return reviewDtoConverter.toDetails(createdReview);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ReviewDetailsDto get(@PathVariable("id") Long id) {
        return reviewDtoConverter.toDetails(reviewService.get(id));
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public ReviewDetailsDto update(@Valid @RequestBody ReviewUpdateDto review) {
        return reviewDtoConverter.toDetails(reviewService.update(reviewDtoConverter.fromUpdate(review)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value = "id") Long id) {
        reviewService.delete(id);
    }
}
