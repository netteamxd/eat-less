package xd.netteam.Eat.Less.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import xd.netteam.Eat.Less.model.converter.IngredientDtoConverter;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientCreationDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientDetailsDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientListDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientUpdateDto;
import xd.netteam.Eat.Less.service.IngredientService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/ingredients")
public class IngredientsController {
    private IngredientService ingredientService;

    private IngredientDtoConverter ingredientDtoConverter;

    @Autowired
    public IngredientsController(IngredientService ingredientService, IngredientDtoConverter ingredientDtoConverter) {
        this.ingredientService = ingredientService;
        this.ingredientDtoConverter = ingredientDtoConverter;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<IngredientListDto> getAll() {
        return ingredientService.getAll().stream()
                .map(ingredientDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(path = "/paginated", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Page<IngredientListDto> getPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ingredientService.getPaginated(page, size).map(ingredientDtoConverter::toList);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public IngredientDetailsDto get(@PathVariable("id") Long id) {
        return ingredientDtoConverter.toDetails(ingredientService.get(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public IngredientDetailsDto create(@Valid @RequestBody IngredientCreationDto ingredient) {
        return ingredientDtoConverter.toDetails(ingredientService.create(ingredientDtoConverter.fromCreation(ingredient)));
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public IngredientDetailsDto update(@Valid @RequestBody IngredientUpdateDto ingredient) {
        return ingredientDtoConverter.toDetails(ingredientService.update(ingredientDtoConverter.fromUpdate(ingredient)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        ingredientService.delete(id);
    }
}
