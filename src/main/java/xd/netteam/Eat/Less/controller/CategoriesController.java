package xd.netteam.Eat.Less.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xd.netteam.Eat.Less.model.converter.CategoryDtoConverter;
import xd.netteam.Eat.Less.model.dto.category.*;
import xd.netteam.Eat.Less.model.entity.Category;
import xd.netteam.Eat.Less.service.CategoryService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/categories")
public class CategoriesController {
    private CategoryService categoryService;
    private CategoryDtoConverter categoryDtoConverter;

    @Autowired
    public CategoriesController(CategoryService categoryService, CategoryDtoConverter categoryDtoConverter) {
        this.categoryService = categoryService;
        this.categoryDtoConverter = categoryDtoConverter;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<CategoryListDto> getAll() {
        return categoryService.getAll().stream()
                .map(categoryDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(path = "/lowest", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<CategoryListDto> lowestCategories() {
        return categoryService.getLowest().stream()
                .map(categoryDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public CategoryDetailsDto get(@PathVariable("id") Long id) {
        return categoryDtoConverter.toDetails(categoryService.get(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public CategoryDetailsDto create(@Valid @RequestBody CategoryCreationDto category) {
        Category creation = categoryDtoConverter.fromCreation(category);
        return categoryDtoConverter.toDetails(categoryService.create(creation));
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public CategoryDetailsDto update(@Valid @RequestBody CategoryUpdateDto category) {
        return categoryDtoConverter.toDetails(categoryService.update(categoryDtoConverter.fromUpdate(category)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value = "id") Long id) {
        categoryService.delete(id);
    }

    @RequestMapping(path = "/origin/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<CategoryListDto> getCategoryGenealogy(@PathVariable(value = "id") Long id){
        return categoryService.getCategoryOrigin(id).stream().map(categoryDtoConverter::toList).collect(Collectors.toList());
    }
}
