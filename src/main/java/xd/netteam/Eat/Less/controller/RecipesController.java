package xd.netteam.Eat.Less.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import xd.netteam.Eat.Less.model.converter.RecipeDtoConverter;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeCreationDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeDetailsDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeListDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeUpdateDto;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.service.CategoryService;
import xd.netteam.Eat.Less.service.RecipeService;
import xd.netteam.Eat.Less.service.UserService;
import xd.netteam.Eat.Less.util.search.SearchSpecificationBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/recipes")
public class RecipesController {
    private RecipeService recipeService;

    private RecipeDtoConverter recipeDtoConverter;

    private CategoryService categoryService;

    private UserService userService;

    @Autowired
    public RecipesController(RecipeService recipeService, RecipeDtoConverter recipeDtoConverter,
                             CategoryService categoryService, UserService userService) {
        this.recipeService = recipeService;
        this.recipeDtoConverter = recipeDtoConverter;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<RecipeListDto> getAll() {
        return recipeService.getAll().stream()
                .map(recipeDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(path = "/paginated", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Page<RecipeListDto> getPaginated(@RequestParam("page") int page, @RequestParam("size") int size) {
        return recipeService.getPaginated(page, size).map(recipeDtoConverter::toList);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public RecipeDetailsDto get(@PathVariable("id") Long id, @AuthenticationPrincipal User user) {
        RecipeDetailsDto recipeDetailsDto = recipeDtoConverter.toDetails(recipeService.get(id));
        if(user != null) {
            boolean isFavourite = userService.getFavouriteRecipes(user.getId()).stream().anyMatch(x -> x.getId().equals(recipeDetailsDto.getId()));
            recipeDetailsDto.setIsFavourite(isFavourite);
        }
        return recipeDetailsDto;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public RecipeDetailsDto create(@Valid @RequestBody RecipeCreationDto recipe, @AuthenticationPrincipal User user) {
        Recipe recipeToCreate = recipeDtoConverter.fromCreation(recipe);
        recipeToCreate.setAuthor(user);
        Recipe createdRecipe = recipeService.create(recipeToCreate);
        return recipeDtoConverter.toDetails(createdRecipe);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public RecipeDetailsDto update(@Valid @RequestBody RecipeUpdateDto recipe) {
        return recipeDtoConverter.toDetails(recipeService.update(recipeDtoConverter.fromUpdate(recipe)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value = "id") Long id) {
        recipeService.delete(id);
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<RecipeListDto> search(@RequestParam Optional<Long> maxCalories,
                                      @RequestParam Optional<Long> minCalories,
                                      @RequestParam Optional<List<Long>> ingredients,
                                      @RequestParam Optional<List<Long>> excluded,
                                      @RequestParam Optional<List<String>> nameKeywords,
                                      @RequestParam Optional<List<Long>> categories) {
        SearchSpecificationBuilder searchSpecificationBuilder = new SearchSpecificationBuilder();
        maxCalories.ifPresent(searchSpecificationBuilder::hasNoMoreCaloriesThan);
        minCalories.ifPresent(searchSpecificationBuilder::hasAtLeastCalories);
        ingredients.ifPresent(list -> list.forEach(searchSpecificationBuilder::containsIngredient));
        excluded.ifPresent(list -> list.forEach(searchSpecificationBuilder::doesNotContainIngredient));
        nameKeywords.ifPresent(list -> list.forEach(searchSpecificationBuilder::nameContains));
        categories.ifPresent(c-> searchSpecificationBuilder.inCategories(c,categoryService));
        return recipeService.search(
                searchSpecificationBuilder.build()
        ).stream().map(recipeDtoConverter::toList).collect(Collectors.toList());
    }

    @RequestMapping(path = "/recommended", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<RecipeListDto> getRecommended(@AuthenticationPrincipal User user) {
        List<Recipe> listOfRecipes = getRecipesBasingOnWhetherUserIsLoggedInOrNot(user);
        return listOfRecipes.stream()
                .map(recipeDtoConverter::toList).collect(Collectors.toList());
    }

    private List<Recipe> getRecipesBasingOnWhetherUserIsLoggedInOrNot(User user) {
        if (user == null)
            return recipeService.getTopRecipesByAverageRating();

        return recipeService.getRecipesRecommendedForUser(user);
    }
}
