package xd.netteam.Eat.Less.controller;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.converter.IngredientDtoConverter;
import xd.netteam.Eat.Less.model.converter.RecipeDtoConverter;
import xd.netteam.Eat.Less.model.converter.UserDtoConverter;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientListDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeListDto;
import xd.netteam.Eat.Less.model.dto.user.UserInfoDto;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class UserController {
    private UserService userService;
    private UserDtoConverter userDtoConverter;
    private IngredientDtoConverter ingredientDtoConverter;
    private RecipeDtoConverter recipeDtoConverter;

    public UserController(UserService userService, UserDtoConverter userDtoConverter, IngredientDtoConverter ingredientDtoConverter,
                          RecipeDtoConverter recipeDtoConverter) {
        this.userService = userService;
        this.userDtoConverter = userDtoConverter;
        this.ingredientDtoConverter = ingredientDtoConverter;
        this.recipeDtoConverter = recipeDtoConverter;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void register(@RequestParam String username, @RequestParam String password) {
        userService.create(username, password);
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public UserInfoDto info(@AuthenticationPrincipal User user) {
        if (user == null)
            throw new UnauthorizedException();
        return userDtoConverter.toInfo(user);
    }

    @RequestMapping(path = "/excludedIngredients", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void excludeIngredient(@AuthenticationPrincipal User user, @RequestParam Long ingredientId) {
        if (user == null)
            throw new UnauthorizedException();
        userService.excludeIngredient(user.getId(), ingredientId);
    }

    @RequestMapping(path = "/excludedIngredients", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void unexcludeIngredient(@AuthenticationPrincipal User user, @RequestParam Long ingredientId) {
        if (user == null)
            throw new UnauthorizedException();
        userService.unexcludeIngredient(user.getId(), ingredientId);
    }

    @RequestMapping(path = "/excludedIngredients", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<IngredientListDto> getExcludedIngredients(@AuthenticationPrincipal User user) {
        if (user != null)
            return userService.getExcludedIngredients(user.getId()).stream().map(ingredientDtoConverter::toList).collect(Collectors.toList());
        else
            return Lists.newArrayList();
    }

    @RequestMapping(path = "/favouriteRecipes", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void addRecipeToFavourites(@AuthenticationPrincipal User user, @RequestParam Long recipeId) {
        if (user == null)
            throw new UnauthorizedException();
        userService.addRecipeToFavourites(user.getId(), recipeId);
    }

    @RequestMapping(path = "/favouriteRecipes", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeRecipeFromFavourites(@AuthenticationPrincipal User user, @RequestParam Long recipeId) {
        if (user == null)
            throw new UnauthorizedException();
        userService.removeRecipeFromFavourites(user.getId(), recipeId);
    }

    @RequestMapping(path = "/favouriteRecipes", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<RecipeListDto> getFavouriteRecipes(@AuthenticationPrincipal User user) {
        if (user != null)
            return userService.getFavouriteRecipes(user.getId()).stream().map(recipeDtoConverter::toList).collect(Collectors.toList());
        else
            return Lists.newArrayList();
    }
}
