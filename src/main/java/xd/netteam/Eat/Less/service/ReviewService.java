package xd.netteam.Eat.Less.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.model.entity.Review;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.repository.RecipeRepository;
import xd.netteam.Eat.Less.repository.ReviewRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ReviewService {
    private ReviewRepository reviewRepository;
    private RecipeRepository recipeRepository;

    @Autowired
    public ReviewService(ReviewRepository reviewRepository, RecipeRepository recipeRepository) {
        this.reviewRepository = reviewRepository;
        this.recipeRepository = recipeRepository;
    }

    public List<Review> getAll() {
        return Lists.newArrayList(reviewRepository.findAll());
    }

    @Transactional
    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public Review create(Review review) {
        Long id = review.getRecipe().getId();
        Recipe recipe = recipeRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        recipe.addReview(review);
        review.setRecipe(recipe);
        Review save = reviewRepository.save(review);
        recipeRepository.save(recipe);
        return save;
    }

    public Review get(Long id) {
        return reviewRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional
    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public Review update(Review review) {
        Review reviewToUpdate = reviewRepository.findById(review.getId()).orElseThrow(ResourceNotFoundException::new);

        reviewToUpdate.setRating(review.getRating());
        reviewToUpdate.setComment(review.getComment());

        return reviewRepository.save(reviewToUpdate);
    }

    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public void delete(Long id) {
        if (reviewRepository.existsById(id)) {
            reviewRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public void deleteAll() {
        reviewRepository.deleteAll();
    }
}
