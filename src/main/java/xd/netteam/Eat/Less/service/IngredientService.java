package xd.netteam.Eat.Less.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.model.entity.Ingredient;
import xd.netteam.Eat.Less.repository.IngredientRepository;

import java.util.List;

@Service
@CacheConfig(cacheNames={"ingredients"})
public class IngredientService {

    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Cacheable
    public List<Ingredient> getAll() {
        return Lists.newArrayList(ingredientRepository.findAll());
    }

    @Cacheable
    public Ingredient get(Long id) {
        return ingredientRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @CacheEvict(allEntries = true)
    public Ingredient create(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }

    public Ingredient getByName(String name) {
        return ingredientRepository.findByName(name);
    }

    @CacheEvict(allEntries = true)
    public Ingredient update(Ingredient ingredient) {
        if (ingredientRepository.existsById(ingredient.getId())) {
            return ingredientRepository.save(ingredient);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @CacheEvict(allEntries = true)
    public void delete(Long id) {
        if (ingredientRepository.existsById(id)) {
            ingredientRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @CacheEvict(allEntries = true)
    public void deleteAll() {
        ingredientRepository.deleteAll();
    }

    public Page<Ingredient> getPaginated(int page, int size) {
        return ingredientRepository.findAll(PageRequest.of(page, size));
    }
}
