package xd.netteam.Eat.Less.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.model.entity.Category;
import xd.netteam.Eat.Less.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@CacheConfig(cacheNames={"categories"})
public class CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Cacheable
    public List<Category> getAll() {
        return Lists.newArrayList(categoryRepository.findAll());
    }

    @Cacheable
    public Category get(Long id) {
        Optional<Category> objectById = categoryRepository.findById(id);
        return objectById.orElse(null);

    }

    @CacheEvict(allEntries = true)
    public Category create(Category category) {
        if(category.getParentCategory() != null && category.getParentCategory().getId() != null)
            category.setParentCategory(categoryRepository.findById(category.getParentCategory().getId()).orElseThrow(ResourceNotFoundException::new));
        else
            category.setParentCategory(null);
        return categoryRepository.save(category);
    }

    @CacheEvict(allEntries = true)
    public Category update(Category category) {
        if (categoryRepository.existsById(category.getId())) {
            if(category.getParentCategory() != null && category.getParentCategory().getId() != null)
                category.setParentCategory(categoryRepository.findById(category.getParentCategory().getId()).orElseThrow(ResourceNotFoundException::new));
            else
                category.setParentCategory(null);
            return categoryRepository.save(category);
        }

        throw new ResourceNotFoundException();
    }

    @CacheEvict(allEntries = true)
    public void delete(Long id) {
        if (categoryRepository.existsById(id))
            categoryRepository.deleteById(id);
        else
            throw new ResourceNotFoundException();
    }

    @CacheEvict(allEntries = true)
    public void deleteAll() {
        categoryRepository.deleteAll();
    }

    public List<Category> getLowest() {
        return categoryRepository.findCategoryByChildCategoriesEmpty();
    }

    public List<Category> getCategoryOrigin(Long id) {
        List<Category> categories = new ArrayList<>();
        Category category = get(id);
        while (category.getParentCategory() != null){
            categories.add(category.getParentCategory());
            category = get(category.getParentCategory().getId());
        }
        return categories;
    }
}
