package xd.netteam.Eat.Less.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    void init();

    Resource load(String file);

    String store(MultipartFile file);

    void deleteAll();
}
