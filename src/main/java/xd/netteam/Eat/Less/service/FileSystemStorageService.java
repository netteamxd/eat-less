package xd.netteam.Eat.Less.service;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import xd.netteam.Eat.Less.config.StorageProperties;
import xd.netteam.Eat.Less.exception.ImageNotFoundException;
import xd.netteam.Eat.Less.exception.StorageException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final Double thumbnailScale;

    @Autowired
    public FileSystemStorageService(StorageProperties storageProperties) {
        this.rootLocation = Paths.get(storageProperties.getLocation());
        this.thumbnailScale = storageProperties.getThumbnailScale();
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException();
        }
    }

    @Override
    public Resource load(String image) {
        try {
            Path file = rootLocation.resolve(image);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new ImageNotFoundException();
            }
        } catch (MalformedURLException e) {
            throw new ImageNotFoundException();
        }
    }

    @Override
    public String store(MultipartFile file) {
        if (file.isEmpty()) {
            throw new StorageException();
        }
        try {
            File img = File.createTempFile("img", "." + FilenameUtils.getExtension(file.getOriginalFilename()), rootLocation.toFile());
            Files.copy(file.getInputStream(), img.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Thumbnails.of(img).scale(thumbnailScale).toFiles(rootLocation.toFile(), Rename.PREFIX_HYPHEN_THUMBNAIL);
            return img.getName();
        } catch (IOException e) {
            throw new StorageException();
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }
}
