package xd.netteam.Eat.Less.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.model.entity.*;
import xd.netteam.Eat.Less.repository.CategoryRepository;
import xd.netteam.Eat.Less.repository.IngredientAmountPerRecipeRepository;
import xd.netteam.Eat.Less.repository.IngredientRepository;
import xd.netteam.Eat.Less.repository.RecipeRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RecipeService {
    private RecipeRepository recipeRepository;
    private CategoryRepository categoryRepository;
    private IngredientRepository ingredientRepository;
    private IngredientAmountPerRecipeRepository ingredientAmountPerRecipeRepository;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository, CategoryRepository categoryRepository, IngredientRepository ingredientRepository, IngredientAmountPerRecipeRepository ingredientAmountPerRecipeRepository) {
        this.recipeRepository = recipeRepository;
        this.categoryRepository = categoryRepository;
        this.ingredientRepository = ingredientRepository;
        this.ingredientAmountPerRecipeRepository = ingredientAmountPerRecipeRepository;
    }

    @Cacheable("recipes")
    public List<Recipe> getAll() {
        return Lists.newArrayList(recipeRepository.findAll());
    }

    @Transactional
    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public Recipe create(Recipe recipe) {
        Set<Category> categoriesToFind = recipe.getCategories();
        Set<Long> categoriesIds = categoriesToFind.stream().map(Category::getId).collect(Collectors.toSet());
        Iterable<Category> categories = categoryRepository.findAllById(categoriesIds);
        recipe.setCategories(Sets.newHashSet(categories));

        Set<IngredientAmountPerRecipe> ingredients = new HashSet<>(recipe.getIngredients());
        recipe.clearIngredients();
        addIngredientsToRecipe(recipe, ingredients);
        return recipeRepository.save(recipe);
    }

    @Cacheable("recipes")
    public Recipe get(Long id) {
        return recipeRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional
    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public Recipe update(Recipe recipe) {
        Recipe recipeToUpdate = recipeRepository.findById(recipe.getId()).orElseThrow(ResourceNotFoundException::new);
        recipeToUpdate.setName(recipe.getName());
        recipeToUpdate.setContent(recipe.getContent());
        recipeToUpdate.setImage(recipe.getImage());
        recipeToUpdate.setPortions(recipe.getPortions());
        recipeToUpdate.setPreparationTime(recipe.getPreparationTime());

        recipeToUpdate.getCategories().clear();
        recipeToUpdate.getIngredients().clear();

        Set<Category> categoriesToFind = recipe.getCategories();
        Set<Long> categoriesIds = categoriesToFind.stream().map(Category::getId).collect(Collectors.toSet());
        categoryRepository.findAllById(categoriesIds).forEach(recipeToUpdate::addCategory);

        Set<IngredientAmountPerRecipe> ingredients = recipe.getIngredients();
        addIngredientsToRecipe(recipeToUpdate, ingredients);

        return recipeRepository.save(recipeToUpdate);
    }

    private void addIngredientsToRecipe(Recipe recipeToUpdate, Set<IngredientAmountPerRecipe> ingredients) {
        for (IngredientAmountPerRecipe iapr : ingredients) {
            Long id = iapr.getIngredient().getId();
            Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
            iapr.setIngredient(ingredient);
            IngredientAmountPerRecipe amountPerRecipe = ingredientAmountPerRecipeRepository.save(iapr);
            recipeToUpdate.addIngredient(amountPerRecipe);
        }
    }

    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public void delete(Long id) {
        if (recipeRepository.existsById(id)) {
            recipeRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @CacheEvict(value = {"recipes", "recipesPaginated", "recipesRecommended"}, allEntries = true)
    public void deleteAll() {
        recipeRepository.deleteAll();
    }

    @Cacheable("recipesPaginated")
    public Page<Recipe> getPaginated(int page, int size) {
        return recipeRepository.findAll(PageRequest.of(page, size));
    }

    @Transactional
    public List<Recipe> search(Specification<Recipe> specification) {
        return recipeRepository.findAll(specification);
    }

    @Cacheable("recipesRecommended")
    public List<Recipe> getRecipesRecommendedForUser(User user){
        return recipeRepository.findAllRecipesRecommendedForUserWithId(user.getId());
    }

    public List<Recipe> getTopRecipesByAverageRating() {
        return recipeRepository.findTop50ByOrderByAverageRating();
    }
}
