package xd.netteam.Eat.Less.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.Diet;
import xd.netteam.Eat.Less.model.DietInfo;
import xd.netteam.Eat.Less.model.entity.Ingredient;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.repository.RecipeRepository;
import xd.netteam.Eat.Less.repository.UserRepository;
import xd.netteam.Eat.Less.util.search.SearchSpecificationBuilder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DietService {
    private static final Integer BREAKFAST_MIN_PERCENT = 25;
    private static final Integer BREAKFAST_MAX_PERCENT = 30;
    private static final Integer SECOND_BREAKFAST_MIN_PERCENT = 5;
    private static final Integer SECOND_BREAKFAST_MAX_PERCENT = 10;
    private static final Integer DINNER_MIN_PERCENT = 30;
    private static final Integer DINNER_MAX_PERCENT = 35;
    private static final Integer TEA_MIN_PERCENT = 5;
    private static final Integer TEA_MAX_PERCENT = 10;
    private static final Integer SUPPER_MIN_PERCENT = 15;
    private static final Integer SUPPER_MAX_PERCENT = 20;

    private RecipeRepository recipeRepository;
    private UserRepository userRepository;

    public DietService(RecipeRepository recipeRepository, UserRepository userRepository) {
        this.recipeRepository = recipeRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public Diet generateDiet(DietInfo dietInfo, User user) {
        Diet diet = new Diet();
        Optional<Recipe> optionalRecipe;
        Long tdee = dietInfo.calculateTotalDailyEnergyExpenditure();
        diet.setTotalCalories(tdee);

        SearchSpecificationBuilder breakfastSpecification = new SearchSpecificationBuilder();
        breakfastSpecification.hasAtLeastCalories(Math.round(tdee * (BREAKFAST_MIN_PERCENT / 100.0)));
        breakfastSpecification.hasNoMoreCaloriesThan(Math.round(tdee * (BREAKFAST_MAX_PERCENT / 100.0)));

        SearchSpecificationBuilder secondBreakfastSpecification = new SearchSpecificationBuilder();
        secondBreakfastSpecification.hasAtLeastCalories(Math.round(tdee * (SECOND_BREAKFAST_MIN_PERCENT / 100.0)));
        secondBreakfastSpecification.hasNoMoreCaloriesThan(Math.round(tdee * (SECOND_BREAKFAST_MAX_PERCENT / 100.0)));

        SearchSpecificationBuilder dinnerSpecification = new SearchSpecificationBuilder();
        dinnerSpecification.hasAtLeastCalories(Math.round(tdee * (DINNER_MIN_PERCENT / 100.0)));
        dinnerSpecification.hasNoMoreCaloriesThan(Math.round(tdee * (DINNER_MAX_PERCENT / 100.0)));

        SearchSpecificationBuilder teaSpecification = new SearchSpecificationBuilder();
        teaSpecification.hasAtLeastCalories(Math.round(tdee * (TEA_MIN_PERCENT / 100.0)));
        teaSpecification.hasNoMoreCaloriesThan(Math.round(tdee * (TEA_MAX_PERCENT / 100.0)));

        SearchSpecificationBuilder supperSpecification = new SearchSpecificationBuilder();
        supperSpecification.hasAtLeastCalories(Math.round(tdee * (SUPPER_MIN_PERCENT / 100.0)));
        supperSpecification.hasNoMoreCaloriesThan(Math.round(tdee * (SUPPER_MAX_PERCENT / 100.0)));

        if(user != null) {
            user = userRepository.findById(user.getId()).orElseThrow(UnauthorizedException::new);
            Set<Ingredient> excludedIngredients = user.getExcludedIngredients();
            List<Long> excludedIds = excludedIngredients.stream().map(Ingredient::getId).collect(Collectors.toList());
            breakfastSpecification.notIn(excludedIds);
            secondBreakfastSpecification.notIn(excludedIds);
            dinnerSpecification.notIn(excludedIds);
            teaSpecification.notIn(excludedIds);
            supperSpecification.notIn(excludedIds);
        }


        optionalRecipe = recipeRepository.findAll(breakfastSpecification.build()).stream().findAny();
        if (optionalRecipe.isPresent()) {
            diet.addMeal(Diet.MealType.BREAKFAST, optionalRecipe.get());
        } else {
            String msg = "BREAKFAST " + Math.round(tdee * (BREAKFAST_MIN_PERCENT / 100.0)) + "-" +
                    Math.round(tdee * (BREAKFAST_MAX_PERCENT / 100.0)) +
                    " not found";
            log.info(msg);
            throw new ResourceNotFoundException(msg);
        }

        secondBreakfastSpecification.notIn(diet.getListOfCurrentRecipeIds());
        optionalRecipe = recipeRepository.findAll(secondBreakfastSpecification.build()).stream().findAny();
        if (optionalRecipe.isPresent()) {
            diet.addMeal(Diet.MealType.SECOND_BREAKFAST, optionalRecipe.get());
        } else {
            String msg = "SECOND_BREAKFAST " + Math.round(tdee * (SECOND_BREAKFAST_MIN_PERCENT / 100.0)) + "-" +
                    Math.round(tdee * (SECOND_BREAKFAST_MAX_PERCENT / 100.0)) +
                    " not found";
            log.info(msg);
            throw new ResourceNotFoundException(msg);
        }

        dinnerSpecification.notIn(diet.getListOfCurrentRecipeIds());
        optionalRecipe = recipeRepository.findAll(dinnerSpecification.build()).stream().findAny();
        if (optionalRecipe.isPresent()) {
            diet.addMeal(Diet.MealType.DINNER, optionalRecipe.get());
        } else {
            String msg = "DINNER " + Math.round(tdee * (DINNER_MIN_PERCENT / 100.0)) + "-" +
                    Math.round(tdee * (DINNER_MAX_PERCENT / 100.0)) +
                    " not found";
            log.info(msg);
            throw new ResourceNotFoundException(msg);
        }


        teaSpecification.notIn(diet.getListOfCurrentRecipeIds());
        optionalRecipe = recipeRepository.findAll(teaSpecification.build()).stream().findAny();
        if (optionalRecipe.isPresent()) {
            diet.addMeal(Diet.MealType.TEA, optionalRecipe.get());
        } else {
            String msg = "TEA " + Math.round(tdee * (TEA_MIN_PERCENT / 100.0)) + "-" +
                    Math.round(tdee * (TEA_MAX_PERCENT / 100.0)) +
                    " not found";
            log.info(msg);
            throw new ResourceNotFoundException(msg);
        }


        supperSpecification.notIn(diet.getListOfCurrentRecipeIds());
        optionalRecipe = recipeRepository.findAll(supperSpecification.build()).stream().findAny();
        if (optionalRecipe.isPresent()) {
            diet.addMeal(Diet.MealType.SUPPER, optionalRecipe.get());
        } else {
            String msg = "SUPPER " + Math.round(tdee * (SUPPER_MIN_PERCENT / 100.0)) + "-" +
                    Math.round(tdee * (SUPPER_MAX_PERCENT / 100.0)) +
                    " not found";
            log.info(msg);
            throw new ResourceNotFoundException(msg);
        }

        return diet;
    }
}
