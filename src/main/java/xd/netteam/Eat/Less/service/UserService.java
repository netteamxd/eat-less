package xd.netteam.Eat.Less.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import xd.netteam.Eat.Less.exception.BadRequestException;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientListDto;
import xd.netteam.Eat.Less.model.entity.Ingredient;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.exception.ResourceNotFoundException;
import xd.netteam.Eat.Less.exception.UserAlreadyExistsException;
import xd.netteam.Eat.Less.repository.UserRepository;
import xd.netteam.Eat.Less.util.results.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private IngredientService ingredientService;
    private RecipeService recipeService;

    public UserService(UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder, IngredientService ingredientService,
                       RecipeService recipeService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.ingredientService = ingredientService;
        this.recipeService = recipeService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("User does not exist");
        }
        return user;
    }

    public List<User> getAll() {
        return Lists.newArrayList(userRepository.findAll());
    }

    public User create(String username, String password) {
        if (userRepository.existsByUsername(username))
            throw new UserAlreadyExistsException();

        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setAdmin(false);
        return userRepository.save(user);
    }

    public User get(Long id) {
        return userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public User update(User user) {
        if (userRepository.existsById(user.getId())) {
            return userRepository.save(user);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    public void delete(Long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    public void deleteAll() {
        userRepository.deleteAll();
    }

    public void excludeIngredient(Long userId, Long ingredientId) {
        User user = get(userId);
        Ingredient ingredient = ingredientService.get(ingredientId);
        if(!user.exclude(ingredient).succeed()){
            throw new BadRequestException();
        }
        update(user);
    }

    public void unexcludeIngredient(Long userId, Long ingredientId) {
        User user = get(userId);
        Ingredient ingredient = ingredientService.get(ingredientId);
        if(!user.unexclude(ingredient).succeed()){
            throw new BadRequestException();
        }
        update(user);
    }

    public List<Ingredient> getExcludedIngredients(Long userId)
    {
        User user = get(userId);
        return new ArrayList<>(user.getExcludedIngredients());
    }

    public void addRecipeToFavourites(Long userId, Long recipeId) {
        User user = get(userId);
        Recipe recipe = recipeService.get(recipeId);
        if(!user.addToFavourites(recipe).succeed()){
            throw  new BadRequestException();
        }
        update(user);
    }

    public void removeRecipeFromFavourites(Long userId, Long recipeId) {
        User user = get(userId);
        Recipe recipe = recipeService.get(recipeId);
        if(!user.removeFromFavourites(recipe).succeed()){
            throw new BadRequestException();
        }
        update(user);
    }

    public Set<Recipe> getFavouriteRecipes(Long userId) {
        return get(userId).getFavouriteRecipes();
    }
}
