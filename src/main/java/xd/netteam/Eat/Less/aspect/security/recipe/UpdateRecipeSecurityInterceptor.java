package xd.netteam.Eat.Less.aspect.security.recipe;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.service.RecipeService;

@Aspect
@Component
public class UpdateRecipeSecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.RecipeService.update(..)) && target(recipeService)")
    public Object checkIfUserIsLoggedInForUpdatingRecipe(ProceedingJoinPoint joinPoint, RecipeService recipeService) throws Throwable {
        Recipe passedRecipe = ((Recipe) joinPoint.getArgs()[0]);
        Recipe recipe = recipeService.get(passedRecipe.getId());
        if (recipe.getAuthor().getUsername().equals(getUsername()))
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
