package xd.netteam.Eat.Less.aspect.security.review;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;

@Aspect
@Component
public class GetReviewSecurityInterceptor extends SecurityInterceptor {
    // RODO
    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.getAll())")
    public Object checkIfUserIsLoggedInForGettingListOfReviews(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAuthenticated())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.get(..))")
    public Object checkIfUserIsLoggedInForGettingReview(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
