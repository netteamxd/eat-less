package xd.netteam.Eat.Less.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.exception.BannedWordsDetectedException;
import xd.netteam.Eat.Less.model.entity.IngredientAmountPerRecipe;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.model.entity.Review;

@Aspect
@Component
public class BannedWordsInterceptor {
    private final String[] bannedWords = {
      "shit",
      "fuck",
      "hooy"
    };

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.create(..)) ||" +
            "execution(* xd.netteam.Eat.Less.service.ReviewService.update(..))")
    public Object checkReviewsForBannedWords(ProceedingJoinPoint joinPoint) throws Throwable {
        Review review = (Review) joinPoint.getArgs()[0];
        if (containsBannedWords(review.getComment()))
            throw new BannedWordsDetectedException();

        return joinPoint.proceed();
    }

    @Around("execution(* xd.netteam.Eat.Less.service.RecipeService.create(..)) ||" +
            "execution(* xd.netteam.Eat.Less.service.RecipeService.update(..))")
    public Object checkRecipesForBannedWords(ProceedingJoinPoint joinPoint) throws Throwable {
        Recipe recipe = (Recipe)joinPoint.getArgs()[0];
        if (containsBannedWords(recipe.getName()) ||
                containsBannedWords(recipe.getContent()))
            throw new BannedWordsDetectedException();

        for(IngredientAmountPerRecipe ingredient : recipe.getIngredients())
            if (containsBannedWords(ingredient.getAmount()))
                throw new BannedWordsDetectedException();

        return joinPoint.proceed();
    }

    @Around("execution(* xd.netteam.Eat.Less.service.UserService.create(..))")
    public Object checkUserForBannedWords(ProceedingJoinPoint joinPoint) throws Throwable {
        String username = joinPoint.getArgs()[0].toString();
        if (containsBannedWords(username))
            throw new BannedWordsDetectedException();

        return joinPoint.proceed();
    }

    private boolean containsBannedWords(String string){
        for (String bannedWord : bannedWords)
            if (string.contains(bannedWord))
                return true;

        return false;
    }
}
