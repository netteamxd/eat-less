package xd.netteam.Eat.Less.aspect.security.review;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.entity.Review;
import xd.netteam.Eat.Less.service.ReviewService;

@Aspect
@Component
public class UpdateReviewSecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.update(..)) && target(reviewService)")
    public Object checkIfUserIsAuthorizedForUpdatingReview(ProceedingJoinPoint joinPoint, ReviewService reviewService) throws Throwable {
        Review passedReview = ((Review) joinPoint.getArgs()[0]);
        Review review = reviewService.get(passedReview.getId());
        if (review.getAuthor().getUsername().equals(getUsername()))
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
