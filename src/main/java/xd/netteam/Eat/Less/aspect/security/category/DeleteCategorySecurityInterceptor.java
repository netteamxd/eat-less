package xd.netteam.Eat.Less.aspect.security.category;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;

@Aspect
@Component
public class DeleteCategorySecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.CategoryService.delete(..))")
    public Object checkIfUserIsAuthorizedForDeletingCategory(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
    @Around("execution(* xd.netteam.Eat.Less.service.CategoryService.deleteAll(..))")
    public Object checkIfUserIsAuthorizedForDeletingAllCategories(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
