package xd.netteam.Eat.Less.aspect.security.review;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;

@Aspect
@Component
public class CreateReviewSecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.create(..))")
    public Object checkIfUserIsAuthorizedForCreatingReview(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAuthenticated())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
