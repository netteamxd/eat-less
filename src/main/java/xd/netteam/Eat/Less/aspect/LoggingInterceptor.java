package xd.netteam.Eat.Less.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Aspect
@Slf4j
@Component
public class LoggingInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.controller.*.*(..))")
    public Object loggingAroundServiceMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        String fullMethodAndClassNameWithParameters = buildMethodAndClassNameWithParameters(joinPoint);

        log.info(formatMessageWithDateTime(fullMethodAndClassNameWithParameters));

        return joinPoint.proceed();
    }

    private String formatMessageWithDateTime(String s) {
        return formatDateTime() + "\t" + s;
    }

    private String formatDateTime(){
        StringBuilder dateTime = new StringBuilder("[");

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss,SSS");
        Date date = new Date();
        dateTime.append(dateFormat.format(date)); //01-06-2018 12:08:43

        dateTime.append("]");

        return dateTime.toString();
    }

    private String buildMethodAndClassNameWithParameters(JoinPoint joinPoint){
        StringBuilder fullClassMethodParamString = new StringBuilder(buildFullMethodAndClassName(joinPoint) + "(");
        fullClassMethodParamString.append(buildParameterString(joinPoint));
        fullClassMethodParamString.append(")");
        return fullClassMethodParamString.toString();
    }

    private String buildParameterString(JoinPoint joinPoint) {
        StringBuilder parameters = new StringBuilder();

        for(Object arg: joinPoint.getArgs())
            parameters.append(arg).append(", ");

        String parameterString = parameters.toString();

        if (parameterString.endsWith(", "))
            parameterString = parameterString.substring(0, parameterString.length() - 2);

        return parameterString;
    }

    private String buildFullMethodAndClassName(JoinPoint joinPoint) {
        return joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();
    }
}
