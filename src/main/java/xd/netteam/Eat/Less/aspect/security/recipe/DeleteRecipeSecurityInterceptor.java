package xd.netteam.Eat.Less.aspect.security.recipe;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.service.RecipeService;

@Aspect
@Component
public class DeleteRecipeSecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.RecipeService.delete(..)) && target(recipeService)")
    public Object checkIfUserIsAuthorizedForDeletingRecipe(ProceedingJoinPoint joinPoint, RecipeService recipeService) throws Throwable {
        Long passedRecipeId = ((Long) joinPoint.getArgs()[0]);
        Recipe recipe = recipeService.get(passedRecipeId);
        if (recipe.getAuthor().getUsername().equals(getUsername()) || isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }

    @Around("execution(* xd.netteam.Eat.Less.service.RecipeService.deleteAll(..))")
    public Object checkIfUserIsAuthorizedForDeletingAllRecipes(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
