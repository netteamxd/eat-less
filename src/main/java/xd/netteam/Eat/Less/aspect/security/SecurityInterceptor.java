package xd.netteam.Eat.Less.aspect.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class SecurityInterceptor {
    protected boolean isAdministrator(){
        return hasAuthority("ROLE_ADMIN");
    }

    protected boolean isAuthenticated(){
        return !hasAuthority("ROLE_ANONYMOUS");
    }

    private boolean hasAuthority(String authority){
        Authentication authentication = getAuthentication();

        if (authentication == null)
            return false;

        return authentication.getAuthorities().stream().anyMatch(x -> x.getAuthority().equals(authority));
    }

    protected String getUsername(){
        Authentication authentication = getAuthentication();

        return authentication.getName();
    }

    private Authentication getAuthentication(){
        SecurityContext context = SecurityContextHolder.getContext();
        return context.getAuthentication();
    }
}
