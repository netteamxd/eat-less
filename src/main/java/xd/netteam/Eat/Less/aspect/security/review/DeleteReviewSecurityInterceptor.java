package xd.netteam.Eat.Less.aspect.security.review;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.aspect.security.SecurityInterceptor;
import xd.netteam.Eat.Less.exception.UnauthorizedException;
import xd.netteam.Eat.Less.model.entity.Review;
import xd.netteam.Eat.Less.service.ReviewService;

@Aspect
@Component
public class DeleteReviewSecurityInterceptor extends SecurityInterceptor {

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.delete(..)) && target(reviewService)")
    public Object checkIfUserIsAuthorizedForDeletingReview(ProceedingJoinPoint joinPoint, ReviewService reviewService) throws Throwable {
        Review review = reviewService.get((Long)joinPoint.getArgs()[0]);
        if (review.getAuthor().getUsername().equals(getUsername()) && isAuthenticated())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }

    @Around("execution(* xd.netteam.Eat.Less.service.ReviewService.deleteAll(..))")
    public Object checkIfUserIsAuthorizedForDeletingAllReviews(ProceedingJoinPoint joinPoint) throws Throwable {
        if (isAdministrator())
            return joinPoint.proceed();

        throw new UnauthorizedException();
    }
}
