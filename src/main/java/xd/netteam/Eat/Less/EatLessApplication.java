package xd.netteam.Eat.Less;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import xd.netteam.Eat.Less.service.StorageService;

@EnableAspectJAutoProxy
@EnableCaching
@SpringBootApplication
public class EatLessApplication {

    public static void main(String[] args) {
        SpringApplication.run(EatLessApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    CommandLineRunner initStorage(StorageService storageService) {
        return (args) -> storageService.init();
    }
}
