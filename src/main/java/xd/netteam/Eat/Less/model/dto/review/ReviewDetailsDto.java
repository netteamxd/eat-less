package xd.netteam.Eat.Less.model.dto.review;

import lombok.Data;
import xd.netteam.Eat.Less.model.dto.user.UserInfoDto;

@Data
public class ReviewDetailsDto {
    private Long id;
    private Integer rating;
    private String comment;
    private UserInfoDto author;
}
