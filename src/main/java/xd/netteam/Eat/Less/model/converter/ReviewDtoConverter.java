package xd.netteam.Eat.Less.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.model.dto.review.ReviewCreationDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewDetailsDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewListDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewUpdateDto;
import xd.netteam.Eat.Less.model.entity.Review;

@Component
public class ReviewDtoConverter implements DtoConverter<Review, ReviewListDto, ReviewDetailsDto, ReviewCreationDto, ReviewUpdateDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public ReviewDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    @Override
    public Review fromCreation(ReviewCreationDto reviewCreationDto) {
        Review review = modelMapper.map(reviewCreationDto, Review.class);
        review.setId(null);
        return review;
    }

    @Override
    public Review fromUpdate(ReviewUpdateDto reviewUpdateDto) {
        return modelMapper.map(reviewUpdateDto, Review.class);
    }

    @Override
    public ReviewListDto toList(Review review) {
        return modelMapper.map(review, ReviewListDto.class);
    }

    @Override
    public ReviewDetailsDto toDetails(Review review) {
        return modelMapper.map(review, ReviewDetailsDto.class);
    }
}
