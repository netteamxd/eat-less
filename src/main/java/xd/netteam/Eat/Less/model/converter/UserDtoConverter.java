package xd.netteam.Eat.Less.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.model.dto.user.UserDetailsDto;
import xd.netteam.Eat.Less.model.dto.user.UserInfoDto;
import xd.netteam.Eat.Less.model.entity.User;

@Component
public class UserDtoConverter {

    private final ModelMapper modelMapper;

    public UserDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public UserInfoDto toInfo(User user) {
        return modelMapper.map(user, UserInfoDto.class);
    }

    public UserDetailsDto toDetails(User user) {
        return modelMapper.map(user, UserDetailsDto.class);
    }
}
