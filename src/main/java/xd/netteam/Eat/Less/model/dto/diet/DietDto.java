package xd.netteam.Eat.Less.model.dto.diet;

import lombok.Data;
import xd.netteam.Eat.Less.model.Diet;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeListDto;

import java.util.Map;

@Data
public class DietDto {
    private Map<Diet.MealType, RecipeListDto> meals;
    private Integer totalCalories;
}
