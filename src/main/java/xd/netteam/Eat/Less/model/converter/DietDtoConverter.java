package xd.netteam.Eat.Less.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.model.Diet;
import xd.netteam.Eat.Less.model.dto.diet.DietDto;

@Component
public class DietDtoConverter {
    private final ModelMapper modelMapper;

    public DietDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public DietDto convert(Diet diet) {
        return modelMapper.map(diet, DietDto.class);
    }
}
