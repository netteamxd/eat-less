package xd.netteam.Eat.Less.model.dto.recipe;

import lombok.Data;
import xd.netteam.Eat.Less.model.dto.category.CategoryForRecipeDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientForRecipeDto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
public class RecipeUpdateDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String content;
    @NotNull
    private String image;
    @NotNull
    private Integer totalCalories;
    @NotNull
    private Integer portions;
    @NotNull
    private Integer preparationTime;
    @NotEmpty
    private List<CategoryForRecipeDto> categories;
    @NotEmpty
    private Set<IngredientForRecipeDto> ingredients;
}
