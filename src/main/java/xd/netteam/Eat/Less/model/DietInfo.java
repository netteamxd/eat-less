package xd.netteam.Eat.Less.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

import static xd.netteam.Eat.Less.model.DietInfo.Gender.MALE;

@Data
public class DietInfo {
    @NotNull
    private Double mass;
    @NotNull
    private Double height;
    @NotNull
    private Integer age;
    @NotNull
    private Gender gender;
    @NotNull
    private Activity activity;

    public enum Gender {
        MALE, FEMALE
    }

    public enum Activity {
        NONE(1.2),
        LOW(1.4),
        MODERATE(1.6),
        HIGH(1.8),
        VERY_HIGH(2.2);

        private final Double value;

        Activity(Double value) {
            this.value = value;
        }

        public Double getValue() {
            return value;
        }
    }

    private Double calculateBasalMetabolicRate() {
        return (10.0 * mass) + (6.25 * height) + (5.0 * age) + (gender.equals(MALE) ? 5 : -161); //Mifflin St Jeor Equation
    }

    public Long calculateTotalDailyEnergyExpenditure() {
        return Math.round(calculateBasalMetabolicRate() * activity.getValue());
    }

}
