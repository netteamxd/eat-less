package xd.netteam.Eat.Less.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeCreationDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeDetailsDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeListDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeUpdateDto;
import xd.netteam.Eat.Less.model.entity.Recipe;

@Component
public class RecipeDtoConverter implements DtoConverter<Recipe, RecipeListDto, RecipeDetailsDto, RecipeCreationDto, RecipeUpdateDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public RecipeDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Recipe fromCreation(RecipeCreationDto recipeCreationDto) {
        Recipe recipe = modelMapper.map(recipeCreationDto, Recipe.class);
        recipe.getIngredients().forEach(i -> i.setId(null));
        return recipe;
    }

    @Override
    public Recipe fromUpdate(RecipeUpdateDto recipeUpdateDto) {
        Recipe recipe = modelMapper.map(recipeUpdateDto, Recipe.class);
        recipe.getIngredients().forEach(i -> i.setId(null));
        return recipe;
    }

    @Override
    public RecipeListDto toList(Recipe recipe) {
        return modelMapper.map(recipe, RecipeListDto.class);
    }

    @Override
    public RecipeDetailsDto toDetails(Recipe recipe) {
        return modelMapper.map(recipe, RecipeDetailsDto.class);
    }
}
