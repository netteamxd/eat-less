package xd.netteam.Eat.Less.model.dto.recipe;

import lombok.Data;
import xd.netteam.Eat.Less.model.dto.category.CategoryListDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientForRecipeDetailsDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewDetailsDto;
import xd.netteam.Eat.Less.model.dto.user.UserInfoDto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class RecipeDetailsDto {
    private Long id;

    private UserInfoDto author;

    private String name;

    private String content;

    private String image;

    private Integer totalCalories;

    private Integer portions;

    private Integer preparationTime;

    private Boolean isFavourite;

    private List<CategoryListDto> categories;

    private Set<IngredientForRecipeDetailsDto> ingredients = new HashSet<>();

    private Set<ReviewDetailsDto> reviews = new HashSet<>();
}
