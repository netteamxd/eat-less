package xd.netteam.Eat.Less.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientCreationDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientDetailsDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientListDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientUpdateDto;
import xd.netteam.Eat.Less.model.entity.Ingredient;

@Component
public class IngredientDtoConverter implements DtoConverter<Ingredient, IngredientListDto, IngredientDetailsDto, IngredientCreationDto, IngredientUpdateDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public IngredientDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Ingredient fromCreation(IngredientCreationDto ingredientCreationDto) {
        return modelMapper.map(ingredientCreationDto, Ingredient.class);
    }

    @Override
    public Ingredient fromUpdate(IngredientUpdateDto ingredientUpdateDto) {
        return modelMapper.map(ingredientUpdateDto, Ingredient.class);
    }

    @Override
    public IngredientListDto toList(Ingredient ingredient) {
        return modelMapper.map(ingredient, IngredientListDto.class);
    }

    @Override
    public IngredientDetailsDto toDetails(Ingredient ingredient) {
        return modelMapper.map(ingredient, IngredientDetailsDto.class);
    }
}
