package xd.netteam.Eat.Less.model;

import lombok.Data;
import xd.netteam.Eat.Less.model.entity.Recipe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class Diet {
    private Map<MealType, Recipe> meals = new HashMap<>();
    private Long totalCalories;

    public void addMeal(MealType mealType, Recipe recipe) {
        meals.put(mealType, recipe);
    }

    public List<Long> getListOfCurrentRecipeIds() {
        return meals.values().stream().map(Recipe::getId).collect(Collectors.toList());
    }

    public enum MealType {
        BREAKFAST,
        SECOND_BREAKFAST,
        DINNER,
        TEA, //podwieczorek
        SUPPER
    }
}
