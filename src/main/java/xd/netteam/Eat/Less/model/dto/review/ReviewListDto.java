package xd.netteam.Eat.Less.model.dto.review;

import lombok.Data;

@Data
public class ReviewListDto {
    private Long id;

    private Integer rating;

    private String comment;
}
