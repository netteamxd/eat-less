package xd.netteam.Eat.Less.model.dto.ingredient;

import lombok.Data;

@Data
public class IngredientForRecipeDetailsDto {
    private IngredientDetailsDto ingredient;
    private String amount;
}
