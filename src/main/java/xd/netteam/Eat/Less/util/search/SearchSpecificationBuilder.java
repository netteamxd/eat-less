package xd.netteam.Eat.Less.util.search;

import org.springframework.data.jpa.domain.Specification;
import xd.netteam.Eat.Less.model.entity.Category;
import xd.netteam.Eat.Less.model.entity.IngredientAmountPerRecipe;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.service.CategoryService;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SearchSpecificationBuilder {

    private Specification<Recipe> specification;

    public SearchSpecificationBuilder() {
        this.specification = (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.conjunction();
    }

    public SearchSpecificationBuilder notIn(List<Long> ids) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.not(root.get("id").in(ids)));

        return this;
    }

    public SearchSpecificationBuilder hasNoMoreCaloriesThan(Long number) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get("totalCalories"), number));

        return this;
    }

    public SearchSpecificationBuilder hasAtLeastCalories(Long number) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get("totalCalories"), number));

        return this;
    }

    public SearchSpecificationBuilder containsIngredient(Long id) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) -> {
            Join<Recipe, IngredientAmountPerRecipe> join = root.join("ingredients");
            return criteriaBuilder.equal(join.get("ingredient").get("id"), id);
        });

        return this;
    }

    public SearchSpecificationBuilder doesNotContainIngredient(Long id) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) -> {
            Subquery<Recipe> sq = criteriaQuery.subquery(Recipe.class);
            Root<Recipe> subqueryRoot = sq.from(Recipe.class);
            sq.select(subqueryRoot.get("id"))
                    .where(((Specification<Recipe>) (subroot, subcriteriaQuery, subcriteriaBuilder) -> {
                        Join<Recipe, IngredientAmountPerRecipe> join = subroot.join("ingredients");
                        return subcriteriaBuilder.equal(join.get("ingredient").get("id"), id);
                    }).toPredicate(subqueryRoot, criteriaQuery, criteriaBuilder));
            return criteriaBuilder.not(
                    criteriaBuilder.in(
                            root.get("id")
                    ).value(sq)
            );
        });

        return this;
    }

    public SearchSpecificationBuilder nameContains(String keyword) {
        specification = specification.and((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), "%" + keyword.toUpperCase() + "%"));

        return this;
    }

    public SearchSpecificationBuilder inCategories(List<Long> categories, CategoryService categoryService) {
        specification = specification.and((Specification<Recipe>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Long> categories1 = new ArrayList<>();
            Stack<Category> categoriesStack = new Stack<>();
            categories.forEach(c -> {categoriesStack.push(categoryService.get(c)); categories1.add(c);});
            while (!categoriesStack.empty()){
                Category category = categoriesStack.pop();
                for(Category child : category.getChildCategories()){
                    categories1.add(child.getId());
                    categoriesStack.push(child);
                }
            }
            Join<Recipe, Category> join = root.join("categories");
            return join.get("id").in(categories1);
        });

        return this;
    }

    private static Specification<Recipe> group() {
        return (root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.groupBy(root.get("id"));
            return criteriaBuilder.conjunction();
        };
    }

    public Specification<Recipe> build() {
        specification = specification.and(group());
        return specification;
    }
}
