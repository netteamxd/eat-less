package xd.netteam.Eat.Less.util.results;

public interface Result {
    boolean succeed();

    String reason();
}
