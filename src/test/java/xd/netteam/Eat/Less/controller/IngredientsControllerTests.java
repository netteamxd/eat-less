package xd.netteam.Eat.Less.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientCreationDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientUpdateDto;
import xd.netteam.Eat.Less.model.entity.Ingredient;
import xd.netteam.Eat.Less.service.IngredientService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WithMockUser(username="test",roles={"USER","ADMIN"})
public class IngredientsControllerTests {
    private static final String API_INGREDIENTS = "/api/ingredients";
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IngredientService ingredientService;

    @Test
    public void getAll() throws Exception {
        given(ingredientService.getAll()).willReturn(createIngredientsList());

        mockMvc.perform(get(API_INGREDIENTS).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].ingredientName", is("Ingredient no 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].ingredientName", is("Ingredient no 2")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].ingredientName", is("Ingredient no 3")))
                .andExpect(jsonPath("$[3].id", is(4)))
                .andExpect(jsonPath("$[3].ingredientName", is("Ingredient no 4")))
                .andExpect(jsonPath("$[4].id", is(5)))
                .andExpect(jsonPath("$[4].ingredientName", is("Ingredient no 5")))
                .andExpect(jsonPath("$[5].id", is(6)))
                .andExpect(jsonPath("$[5].ingredientName", is("Ingredient no 6")))
                .andExpect(jsonPath("$[6].id", is(7)))
                .andExpect(jsonPath("$[6].ingredientName", is("Ingredient no 7")))
                .andExpect(jsonPath("$[7].id", is(8)))
                .andExpect(jsonPath("$[7].ingredientName", is("Ingredient no 8")))
                .andExpect(jsonPath("$[8].id", is(9)))
                .andExpect(jsonPath("$[8].ingredientName", is("Ingredient no 9")))
                .andExpect(jsonPath("$[9].id", is(10)))
                .andExpect(jsonPath("$[9].ingredientName", is("Ingredient no 10")));

        verify(ingredientService, times(1)).getAll();
        verifyNoMoreInteractions(ingredientService);
    }

    private List<Ingredient> createIngredientsList() {
        List<Ingredient> listOfIngredients = new ArrayList<>();

        for(Long i = 1L; i <= 10; i++)
            listOfIngredients.add(createRandomIngredient(i, i % 2 == 0));

        return listOfIngredients;
    }

    @Test
    public void getById() throws Exception {
        given(ingredientService.get(any())).willReturn(createRandomIngredient(1L, true));

        mockMvc.perform(get(API_INGREDIENTS + "/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ingredientName", is("Ingredient no 1")))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.animalProduct", is(true)));

        verify(ingredientService, times(1)).get(1L);
        verifyNoMoreInteractions(ingredientService);
    }

    private Ingredient createRandomIngredient(Long id, boolean isAnimalProduct){
        return createIngredient(id, "Ingredient no " + id.toString(), isAnimalProduct);
    }
    private Ingredient createIngredient(Long id, String name, boolean isAnimalProduct) {
        return new Ingredient(id, name, isAnimalProduct);
    }

    @Test
    public void create() throws Exception {
        given(ingredientService.create(any())).willReturn(createRandomIngredient(1L, true));

        mockMvc.perform(post(API_INGREDIENTS).content(mapper.writeValueAsString(createIngredientCreationDto()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.ingredientName", is("Ingredient no 1")))
                .andExpect(jsonPath("$.animalProduct", is(true)));

        verify(ingredientService, times(1)).create(any());
        verifyNoMoreInteractions(ingredientService);
    }

    private IngredientCreationDto createIngredientCreationDto() {
        IngredientCreationDto ingredient = new IngredientCreationDto();

        ingredient.setAnimalProduct(true);
        ingredient.setIngredientName("Pickle");

        return ingredient;
    }

    @Test
    public void update() throws Exception {
        given(ingredientService.update(any())).willReturn(createRandomIngredient(1L, true));

        mockMvc.perform(put(API_INGREDIENTS).content(mapper.writeValueAsString(createIngredientUpdateDto()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ingredientName", is("Ingredient no 1")))
                .andExpect(jsonPath("$.animalProduct", is(true)));

        verify(ingredientService, times(1)).update(any());
        verifyNoMoreInteractions(ingredientService);
    }

    private IngredientUpdateDto createIngredientUpdateDto() {
        IngredientUpdateDto ingredientUpdateDto = new IngredientUpdateDto();

        ingredientUpdateDto.setAnimalProduct(true);
        ingredientUpdateDto.setIngredientName("Pickle");
        ingredientUpdateDto.setId(1L);

        return ingredientUpdateDto;
    }


    @Test
    public void deleteTest() throws Exception {
        doNothing().when(ingredientService).delete(any(Long.class));

        mockMvc.perform(delete(API_INGREDIENTS +"/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        verify(ingredientService, times(1)).delete(1L);
        verifyNoMoreInteractions(ingredientService);
    }


    @Test
    public void getPaginatedDataTest() throws Exception {
        given(ingredientService.getPaginated(1, 3)).willReturn(createPaginatedResponse());

        mockMvc.perform(get(API_INGREDIENTS + "/paginated?page=1&size=3").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)));

        verify(ingredientService, times(1)).getPaginated(1, 3);
        verifyNoMoreInteractions(ingredientService);
    }

    private Page<Ingredient> createPaginatedResponse() {
        return new PageImpl<>(createIngredientsList().subList(0, 3));
    }
}