package xd.netteam.Eat.Less.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xd.netteam.Eat.Less.model.dto.category.CategoryForRecipeDto;
import xd.netteam.Eat.Less.model.dto.category.CategoryListDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientDetailsDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientForRecipeDetailsDto;
import xd.netteam.Eat.Less.model.dto.ingredient.IngredientForRecipeDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeCreationDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeDetailsDto;
import xd.netteam.Eat.Less.model.dto.recipe.RecipeUpdateDto;
import xd.netteam.Eat.Less.model.dto.review.ReviewDetailsDto;
import xd.netteam.Eat.Less.model.entity.*;
import xd.netteam.Eat.Less.service.RecipeService;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WithMockUser(username="test",roles={"USER","ADMIN"})
public class RecipesControllerTests {
    private static final String API_RECIPES = "/api/recipes";
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeService recipeService;

    @Test
    public void getAll() throws Exception {
        given(recipeService.getAll()).willReturn(createRecipesList());

        mockMvc.perform(get(API_RECIPES).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].name", is("Recipe no 1")))
                .andExpect(jsonPath("$[0].image", is("Image no 1")))
                .andExpect(jsonPath("$[0].totalCalories", is(100)))
                .andExpect(jsonPath("$[0].portions", is(2)))
                .andExpect(jsonPath("$[0].preparationTime", is(40)))
                .andExpect(jsonPath("$[0].averageRating", is(0.0)))
                .andExpect(jsonPath("$[1].name", is("Recipe no 2")))
                .andExpect(jsonPath("$[1].image", is("Image no 2")))
                .andExpect(jsonPath("$[1].totalCalories", is(100)))
                .andExpect(jsonPath("$[1].portions", is(2)))
                .andExpect(jsonPath("$[1].preparationTime", is(40)))
                .andExpect(jsonPath("$[1].averageRating", is(0.0)));

                // etc. ...

        verify(recipeService, times(1)).getAll();
        verifyNoMoreInteractions(recipeService);
    }

    private List<Recipe> createRecipesList() {
        List<Recipe> listOfRecipes = new ArrayList<>();

        for(Long i = 1L; i <= 10; i++)
            listOfRecipes.add(createRecipe(i,
                    "Recipe no " + i.toString(),
                    "Image no " + i.toString(),
                    100,
                    2,
                    40,
                    4.76));


        return listOfRecipes;
    }

    private Recipe createRecipe(Long id,
                                              String name,
                                              String image,
                                              Integer totalCalories,
                                              Integer portions,
                                              Integer preparationTime,
                                              Double avgRating) {
        Recipe recipe = new Recipe();

        recipe.setId(id);
        recipe.setName(name);
        recipe.setImage(image);
        recipe.setTotalCalories(totalCalories);
        recipe.setPortions(portions);
        recipe.setPreparationTime(preparationTime);
        recipe.setContent("Random content");

        return recipe;
    }

    private Recipe createRandomRecipe(Long id){
        return createRecipe(id, "Recipe no " + id.toString(), "Image no " + id.toString(),
                1000, 4, 45, 4.76);
    }


    @Test
    public void getById() throws Exception {
        given(recipeService.get(any())).willReturn(createRandomRecipe(1L));

        mockMvc.perform(get(API_RECIPES + "/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Recipe no 1")))
                .andExpect(jsonPath("$.image", is("Image no 1")))
                .andExpect(jsonPath("$.totalCalories", is(1000)))
                .andExpect(jsonPath("$.content", is("Random content")))
                .andExpect(jsonPath("$.portions", is(4)))
                .andExpect(jsonPath("$.preparationTime", is(45)))
                .andExpect(jsonPath("$.averageRating").doesNotExist());

        verify(recipeService, times(1)).get(1L);
        verifyNoMoreInteractions(recipeService);
    }

    @Test
    public void create() throws Exception {
        given(recipeService.create(any())).willReturn(createRecipeCreationAndUpdateResponse());

        mockMvc.perform(post(API_RECIPES).content(mapper.writeValueAsString(createRecipeCreationDto()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("Test recipe")))
                .andExpect(jsonPath("$.portions", is(5)))
                .andExpect(jsonPath("$.preparationTime", is(45)))
                .andExpect(jsonPath("$.totalCalories", is(2000)))
                .andExpect(jsonPath("$.content", is("Random content")))
                .andExpect(jsonPath("$.image", is("test.jpg")))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].ingredient.ingredientName", is("Pickle")))
                .andExpect(jsonPath("$.ingredients[0].ingredient.id", is(1)))
                .andExpect(jsonPath("$.ingredients[0].ingredient.animalProduct", is(true)))
                .andExpect(jsonPath("$.ingredients[0].amount", is("20 pcs")))
                .andExpect(jsonPath("$.reviews", hasSize(1)))
                .andExpect(jsonPath("$.reviews[0].id", is(1)))
                .andExpect(jsonPath("$.reviews[0].rating", is(5)))
                .andExpect(jsonPath("$.reviews[0].comment", is("Gut gut")))
                .andExpect(jsonPath("$.categories", hasSize(1)))
                .andExpect(jsonPath("$.categories[0].categoryName", is("Dairy-free")));

        verify(recipeService, times(1)).create(any());
        verifyNoMoreInteractions(recipeService);
    }

    private Recipe createRecipeCreationAndUpdateResponse() {
        Recipe recipe = createRecipe(1L,
                "Test recipe", "test.jpg",
                2000, 5, 45, 0.0);

        recipe.addIngredient(new IngredientAmountPerRecipe(new Ingredient(1L, "Pickle", true),  "20 pcs"));
        recipe.addReview(new Review(1L, 5, "Gut gut", null, recipe, true));
        Category category = new Category();
        category.setId(1L);
        category.setName("Dairy-free");
        recipe.addCategory(category);

        return recipe;
    }

    private RecipeCreationDto createRecipeCreationDto() {
        RecipeCreationDto recipeCreationDto = new RecipeCreationDto();

        recipeCreationDto.setPortions(5);
        recipeCreationDto.setPreparationTime(10);
        recipeCreationDto.setContent("test content");
        recipeCreationDto.setTotalCalories(1000);
        recipeCreationDto.setName("Test Recipe");
        recipeCreationDto.setImage("imgTEST.jpg");
        recipeCreationDto.setCategories(createCategoriesForRecipe());
        recipeCreationDto.setIngredients(createIngredientsListForRecipeCreationOrUpdate());

        return recipeCreationDto;
    }

    private Set<IngredientForRecipeDto> createIngredientsListForRecipeCreationOrUpdate() {
        Set<IngredientForRecipeDto> setOfIngredients = new HashSet<>();

        IngredientForRecipeDto ingredient = new IngredientForRecipeDto();
        ingredient.setAmount("20 pcs");
        ingredient.setIngredientId(1L);
        setOfIngredients.add(ingredient);

        return setOfIngredients;
    }

    private List<CategoryForRecipeDto> createCategoriesForRecipe() {
        List<CategoryForRecipeDto> listOfCategories = new ArrayList<>();

        CategoryForRecipeDto category = new CategoryForRecipeDto();
        category.setId(1L);
        listOfCategories.add(category);

        return listOfCategories;
    }

    private Set<ReviewDetailsDto> createReviewsList() {
        Set<ReviewDetailsDto> reviews = new HashSet<>();

        ReviewDetailsDto review = new ReviewDetailsDto();
        review.setId(1L);
        review.setRating(5);
        review.setComment("Gut gut");
        reviews.add(review);

        return reviews;
    }

    private Set<IngredientForRecipeDetailsDto> createIngredientsListForRecipeDetails() {
        Set<IngredientForRecipeDetailsDto> ingredients = new HashSet<>();

        IngredientForRecipeDetailsDto ingredient = new IngredientForRecipeDetailsDto();
        ingredient.setAmount("20 pcs");
        IngredientDetailsDto ingredientDetails = new IngredientDetailsDto();
        ingredientDetails.setAnimalProduct(true);
        ingredientDetails.setId(1L);
        ingredientDetails.setIngredientName("Pickle");
        ingredient.setIngredient(ingredientDetails);

        ingredients.add(ingredient);

        return ingredients;
    }

    private List<CategoryListDto> createCategoryList() {
        List<CategoryListDto> listOfCategories = new ArrayList<>();

        CategoryListDto category = new CategoryListDto();
        category.setId(1L);
        category.setCategoryName("Dairy-free");
        listOfCategories.add(category);

        return listOfCategories;
    }

    @Test
    public void update() throws Exception {
        given(recipeService.update(any())).willReturn(createRecipeCreationAndUpdateResponse());

        mockMvc.perform(put(API_RECIPES).content(mapper.writeValueAsString(createRecipeUpdateDto()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Test recipe")))
                .andExpect(jsonPath("$.portions", is(5)))
                .andExpect(jsonPath("$.preparationTime", is(45)))
                .andExpect(jsonPath("$.totalCalories", is(2000)))
                .andExpect(jsonPath("$.content", is("Random content")))
                .andExpect(jsonPath("$.image", is("test.jpg")))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].ingredient.ingredientName", is("Pickle")))
                .andExpect(jsonPath("$.ingredients[0].ingredient.id", is(1)))
                .andExpect(jsonPath("$.ingredients[0].ingredient.animalProduct", is(true)))
                .andExpect(jsonPath("$.ingredients[0].amount", is("20 pcs")))
                .andExpect(jsonPath("$.reviews", hasSize(1)))
                .andExpect(jsonPath("$.reviews[0].id", is(1)))
                .andExpect(jsonPath("$.reviews[0].rating", is(5)))
                .andExpect(jsonPath("$.reviews[0].comment", is("Gut gut")))
                .andExpect(jsonPath("$.categories", hasSize(1)))
                .andExpect(jsonPath("$.categories[0].categoryName", is("Dairy-free")));

        verify(recipeService, times(1)).update(any());
        verifyNoMoreInteractions(recipeService);
    }

    private RecipeUpdateDto createRecipeUpdateDto() {
        RecipeUpdateDto recipeUpdateDto = new RecipeUpdateDto();

        recipeUpdateDto.setId(1L);
        recipeUpdateDto.setName("Test");
        recipeUpdateDto.setImage("test.jpg");
        recipeUpdateDto.setTotalCalories(400);
        recipeUpdateDto.setPortions(4);
        recipeUpdateDto.setCategories(createCategoriesForRecipe());
        recipeUpdateDto.setIngredients(createIngredientsListForRecipeCreationOrUpdate());
        recipeUpdateDto.setContent("Test");
        recipeUpdateDto.setPreparationTime(45);

        return recipeUpdateDto;
    }

    @Test
    public void deleteTest() throws Exception {
        doNothing().when(recipeService).delete(any(Long.class));

        mockMvc.perform(delete(API_RECIPES +"/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        verify(recipeService, times(1)).delete(1L);
        verifyNoMoreInteractions(recipeService);
    }

    @Test
    public void getPaginatedDataTest() throws Exception {
        given(recipeService.getPaginated(1, 3)).willReturn(createPaginatedResponse());

        mockMvc.perform(get(API_RECIPES + "/paginated?page=1&size=3").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)));

        verify(recipeService, times(1)).getPaginated(1, 3);
        verifyNoMoreInteractions(recipeService);
    }

    private Page<Recipe> createPaginatedResponse() {
        return new PageImpl<>(createRecipesList().subList(0, 3));
    }


    @Test
    public void getRecommendedRecipesForNonLoggedInUser() throws Exception {
        given(recipeService.getTopRecipesByAverageRating()).willReturn(createRecipesList());

        mockMvc.perform(get(API_RECIPES + "/recommended").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].name", is("Recipe no 1")))
                .andExpect(jsonPath("$[0].image", is("Image no 1")))
                .andExpect(jsonPath("$[0].totalCalories", is(100)))
                .andExpect(jsonPath("$[0].portions", is(2)))
                .andExpect(jsonPath("$[0].preparationTime", is(40)))
                .andExpect(jsonPath("$[0].averageRating", is(0.0)))
                .andExpect(jsonPath("$[1].name", is("Recipe no 2")))
                .andExpect(jsonPath("$[1].image", is("Image no 2")))
                .andExpect(jsonPath("$[1].totalCalories", is(100)))
                .andExpect(jsonPath("$[1].portions", is(2)))
                .andExpect(jsonPath("$[1].preparationTime", is(40)))
                .andExpect(jsonPath("$[1].averageRating", is(0.0)));

        // etc. ...

        verify(recipeService, times(1)).getTopRecipesByAverageRating();
        verifyNoMoreInteractions(recipeService);
    }

    @Test
    public void getRecommendedRecipesForLoggedInUser() throws Exception {
        given(recipeService.getRecipesRecommendedForUser(any())).willReturn(createRecipesList());

        User user = new User();
        mockMvc.perform(get(API_RECIPES + "/recommended")
                .with(user(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].name", is("Recipe no 1")))
                .andExpect(jsonPath("$[0].image", is("Image no 1")))
                .andExpect(jsonPath("$[0].totalCalories", is(100)))
                .andExpect(jsonPath("$[0].portions", is(2)))
                .andExpect(jsonPath("$[0].preparationTime", is(40)))
                .andExpect(jsonPath("$[0].averageRating", is(0.0)))
                .andExpect(jsonPath("$[1].name", is("Recipe no 2")))
                .andExpect(jsonPath("$[1].image", is("Image no 2")))
                .andExpect(jsonPath("$[1].totalCalories", is(100)))
                .andExpect(jsonPath("$[1].portions", is(2)))
                .andExpect(jsonPath("$[1].preparationTime", is(40)))
                .andExpect(jsonPath("$[1].averageRating", is(0.0)));

        // etc. ...

        verify(recipeService, times(1)).getRecipesRecommendedForUser(any());
        verifyNoMoreInteractions(recipeService);
    }
}