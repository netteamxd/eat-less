package xd.netteam.Eat.Less.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xd.netteam.Eat.Less.model.entity.User;
import xd.netteam.Eat.Less.service.UserService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UsersControllerTests {
    private static final String API_USERS = "/api/auth";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;


    @Test
    public void registrationTest() throws Exception {
        given(userService.create("test", "test"))
                .willReturn(createUser("test", "test"));

        mockMvc.perform(post(API_USERS + "/register")
                .param("username", "test")
                .param("password", "test")
                .content("")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        verify(userService, times(1)).create("test", "test");
        verifyNoMoreInteractions(userService);
    }

    private User createUser(String login, String password) {
        User user = new User();
        user.setUsername(login);
        user.setPassword(password);
        return user;
    }
}