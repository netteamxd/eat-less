package xd.netteam.Eat.Less.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xd.netteam.Eat.Less.model.dto.review.ReviewCreationDto;
import xd.netteam.Eat.Less.model.entity.Review;
import xd.netteam.Eat.Less.service.ReviewService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WithMockUser(username="test",roles={"USER","ADMIN"})
public class ReviewsControllerTests {
    private static final String API_REVIEWS = "/api/reviews";
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @Test
    public void getAll() throws Exception {
        given(reviewService.getAll()).willReturn(createReviewsList());

        mockMvc.perform(get(API_REVIEWS).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].comment", is("Review no 1")))
                .andExpect(jsonPath("$[0].rating", is(2)))
                .andExpect(jsonPath("$[1].comment", is("Review no 2")))
                .andExpect(jsonPath("$[1].rating", is(3)))
                .andExpect(jsonPath("$[2].comment", is("Review no 3")))
                .andExpect(jsonPath("$[2].rating", is(4)))
                .andExpect(jsonPath("$[3].comment", is("Review no 4")))
                .andExpect(jsonPath("$[3].rating", is(5)))
                .andExpect(jsonPath("$[4].comment", is("Review no 5")))
                .andExpect(jsonPath("$[4].rating", is(1)))
                .andExpect(jsonPath("$[5].comment", is("Review no 6")))
                .andExpect(jsonPath("$[5].rating", is(2)))
                .andExpect(jsonPath("$[6].comment", is("Review no 7")))
                .andExpect(jsonPath("$[6].rating", is(3)))
                .andExpect(jsonPath("$[7].comment", is("Review no 8")))
                .andExpect(jsonPath("$[7].rating", is(4)))
                .andExpect(jsonPath("$[8].comment", is("Review no 9")))
                .andExpect(jsonPath("$[8].rating", is(5)))
                .andExpect(jsonPath("$[9].comment", is("Review no 10")))
                .andExpect(jsonPath("$[9].rating", is(1)));

        verify(reviewService, times(1)).getAll();
        verifyNoMoreInteractions(reviewService);
    }

    private List<Review> createReviewsList() {
        List<Review> listOfReviews = new ArrayList<>();
        for(Long i = 1L; i <= 10L; i++)
            listOfReviews.add(createReview(i, Math.toIntExact(i % 5) + 1));
        return listOfReviews;
    }

    @Test
    public void getById() throws Exception {
        given(reviewService.get(any())).willReturn(createReview(1L, 3));

        mockMvc.perform(get(API_REVIEWS + "/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment", is("Review no 1")))
                .andExpect(jsonPath("$.rating", is(3)));

        verify(reviewService, times(1)).get(1L);
        verifyNoMoreInteractions(reviewService);
    }

    @Test
    public void create() throws Exception {
        given(reviewService.create(any())).willReturn(createReview(1L, 3));

        mockMvc.perform(post(API_REVIEWS).content(mapper.writeValueAsString(createReviewCreationDto()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.comment", is("Review no 1")))
                .andExpect(jsonPath("$.rating", is(3)));

        verify(reviewService, times(1)).create(any());
        verifyNoMoreInteractions(reviewService);
    }

    private ReviewCreationDto createReviewCreationDto() {
        return new ReviewCreationDto(3, "comment", 1L);
    }

    @Test
    public void update() throws Exception {
        given(reviewService.update(any())).willReturn(createReview(1L, 2));

        mockMvc.perform(put(API_REVIEWS).content(mapper.writeValueAsString(createReview(1L, 3)))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment", is("Review no 1")))
                .andExpect(jsonPath("$.rating", is(2)));

        verify(reviewService, times(1)).update(any());
        verifyNoMoreInteractions(reviewService);
    }

    private Review createReview(Long id, Integer rating) {
        return new Review(id, rating, "Review no " + id.toString(), null, null, true);
    }


    @Test
    public void deleteTest() throws Exception {
        doNothing().when(reviewService).delete(any(Long.class));

        mockMvc.perform(delete(API_REVIEWS +"/1").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        verify(reviewService, times(1)).delete(1L);
        verifyNoMoreInteractions(reviewService);
    }
}