package xd.netteam.Eat.Less.service;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.*;
import xd.netteam.Eat.Less.repository.RecipeRepository;
import xd.netteam.Eat.Less.repository.ReviewRepository;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ReviewServiceTests {
    @Autowired
    private ReviewService reviewService;

    @MockBean
    private ReviewRepository reviewRepository;
    @MockBean
    private RecipeRepository recipeRepository;


    @Test
    @WithMockUser(username="test",roles={"USER","ADMIN"})
    public void get() {
        given(reviewRepository.findById(1L)).willReturn(Optional.of(createTestReview(1L, createTestRecipe(1L))));

        Review review = reviewService.get(1L);

        assertThat(review.getId(), is(1L));

        verify(reviewRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    public void getAll() {
        given(reviewRepository.findAll()).willReturn(createTestReviewsList());

        List<Review> reviews = reviewService.getAll();

        assertTrue(reviews.size() > 1);

        verify(reviewRepository, times(1)).findAll();
        verifyNoMoreInteractions(reviewRepository);
    }

    private Iterable<Review> createTestReviewsList() {
        List<Review> reviews = new ArrayList<>();

        for(Long i = 0L; i< 10L; i++)
            reviews.add(createTestReview(1L, createTestRecipe(1L)));

        return reviews;
    }

    @Test
    public void create() {
        given(reviewRepository.save(any())).willReturn(createTestReview(1L, createTestRecipe(1L)));
        given(recipeRepository.findById(any())).willReturn(Optional.of(createTestRecipe(1L)));

        Recipe recipe = createTestRecipe(1L);
        Review review = reviewService.create(createTestReview(1L, recipe));

        assertThat(review.getId(), is(1L));
        assertThat(review.getComment(), is("comment"));
        assertThat(review.getRecipe(), is(recipe));

        verify(reviewRepository, times(1)).save(any());
        verifyNoMoreInteractions(reviewRepository);
    }

    @Test
    @WithMockUser(username="test",roles={"USER","ADMIN"})
    public void update() {
        given(reviewRepository.findById(any())).willReturn(Optional.of(createTestReview(1L, createTestRecipe(1L))));
        given(reviewRepository.save(any())).willAnswer(a -> a.getArgument(0));

        Review testReview = createTestReview(1L, createTestRecipe(1L));
        testReview.setComment("new comment");
        Review review = reviewService.update(testReview);

        assertThat(review.getId(), is(1L));
        assertThat(review.getComment(), is("new comment"));
        assertThat(review.getRating(), is(3));

        verify(reviewRepository, times(1)).save(any());
        verify(reviewRepository, times(2)).findById(any());
        verifyNoMoreInteractions(reviewRepository);
    }

    private Review createTestReview(Long id, Recipe recipe) {
        User author = new User();
        author.setUsername("test");
        return new Review(id, 3, "comment", author, recipe, true);
    }

    private Recipe createTestRecipe(Long id) {
        Recipe recipe = new Recipe();
        recipe.setId(id);
        recipe.setPortions(4);
        recipe.setPreparationTime(10);
        recipe.setContent("test content");
        recipe.setTotalCalories(1000);
        recipe.setName("Test Recipe");
        recipe.setImage("imgTEST.jpg");
        recipe.setIngredients(Sets.newHashSet(new IngredientAmountPerRecipe(1L, new Ingredient(1L, "test ingr", true), "10g")));
        recipe.setCategories(createTestCategories());
        return recipe;
    }

    private HashSet<Category> createTestCategories() {
        return Sets.newHashSet(new Category(1L, "Category", null, Sets.newHashSet(), Sets.newHashSet()));
    }
}