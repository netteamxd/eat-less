package xd.netteam.Eat.Less.service;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.Category;
import xd.netteam.Eat.Less.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryServiceTests {

    @Autowired
    private CategoryService categoryService;
    @MockBean
    private CategoryRepository categoryRepository;


    @Test
    public void get() {
        given(categoryRepository.findById(1L)).willReturn(Optional.of(createTestCategory(1L)));

        Category category = categoryService.get(1L);

        assertThat(category.getId(), is(1L));

        verify(categoryRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(categoryRepository);
    }

    private Category createTestCategory(Long id) {
        Category category = new Category();

        category.setId(id);
        category.setName("Test category no " + id.toString());
        category.setChildCategories(Sets.newHashSet());
        category.setRecipes(Sets.newHashSet());
        category.setParentCategory(null);

        return category;
    }

    @Test
    public void getAll() {
        given(categoryRepository.findAll()).willReturn(createTestCategoriesList());

        List<Category> categories = categoryService.getAll();

        assertTrue(categories.size() > 1);

        verify(categoryRepository, times(1)).findAll();
        verifyNoMoreInteractions(categoryRepository);
    }

    private Iterable<Category> createTestCategoriesList() {
        List<Category> categories = new ArrayList<>();

        for(Long i = 0L; i < 10L; i++)
            categories.add(createTestCategory(i));

        return categories;
    }

    @Test
    @WithMockUser(username="test",roles={"USER","ADMIN"})
    public void create() {
        given(categoryRepository.save(any())).willReturn(createTestCategory(1L));

        Category category = categoryService.create(createTestCategory(1L));

        assertThat(category.getId(), is(1L));
        assertThat(category.getName(), is("Test category no 1"));

        verify(categoryRepository, times(1)).save(any());
        verifyNoMoreInteractions(categoryRepository);
    }

    @Test
    @WithMockUser(username="test",roles={"USER","ADMIN"})
    public void update() {
        given(categoryRepository.existsById(any())).willReturn(true);
        given(categoryRepository.save(any())).willAnswer(a -> a.getArgument(0));

        Category category = createTestCategory(1L);
        category.setName("Test category no 2");
        Category updatedCategory = categoryService.update(category);

        assertThat(updatedCategory.getId(), is(1L));
        assertThat(updatedCategory.getName(), is("Test category no 2"));

        verify(categoryRepository, times(1)).save(any());
        verify(categoryRepository, times(1)).existsById(any());
        verifyNoMoreInteractions(categoryRepository);
    }
}
