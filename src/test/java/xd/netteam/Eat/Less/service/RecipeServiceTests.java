package xd.netteam.Eat.Less.service;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.*;
import xd.netteam.Eat.Less.repository.CategoryRepository;
import xd.netteam.Eat.Less.repository.IngredientAmountPerRecipeRepository;
import xd.netteam.Eat.Less.repository.IngredientRepository;
import xd.netteam.Eat.Less.repository.RecipeRepository;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RecipeServiceTests {

    @Autowired
    private RecipeService recipeService;

    @MockBean
    private RecipeRepository recipeRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private IngredientRepository ingredientRepository;
    @MockBean
    private IngredientAmountPerRecipeRepository ingredientAmountPerRecipeRepository;

    private Recipe createTestRecipe(Long id) {
        Recipe recipe = new Recipe();
        recipe.setId(id);
        recipe.setPortions(4);
        recipe.setPreparationTime(10);
        recipe.setContent("test content");
        recipe.setTotalCalories(1000);
        recipe.setName("Test Recipe");
        recipe.setImage("imgTEST.jpg");
        recipe.setIngredients(Sets.newHashSet(new IngredientAmountPerRecipe(1L, new Ingredient(1L, "test ingr", true), "10g")));
        recipe.setCategories(createTestCategories());
        User author = new User();
        author.setUsername("test");
        recipe.setAuthor(author);
        return recipe;
    }

    private HashSet<Category> createTestCategories() {
        return Sets.newHashSet(new Category(1L, "Category", null, Sets.newHashSet(), Sets.newHashSet()));
    }

    @Test
    public void get() {
        given(recipeRepository.findById(1L)).willReturn(Optional.of(createTestRecipe(1L)));

        Recipe recipe = recipeService.get(1L);

        assertThat(recipe.getId(), is(1L));
        assertThat(recipe.getName(), is("Test Recipe"));

        verify(recipeRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(recipeRepository);
    }

    @Test
    public void getRecommendedForUser() {
        given(recipeRepository.findAllRecipesRecommendedForUserWithId(any())).willReturn(createTestRecipesList());

        User user = new User();
        List<Recipe> recipes = recipeService.getRecipesRecommendedForUser(user);

        assertTrue(recipes.size() > 1);

        verify(recipeRepository, times(1)).findAllRecipesRecommendedForUserWithId(any());
        verifyNoMoreInteractions(recipeRepository);
    }

    @Test
    public void getTop50RecipesBasedOnAverageRating() {
        given(recipeRepository.findTop50ByOrderByAverageRating()).willReturn(createTestRecipesList());

        List<Recipe> recipes = recipeService.getTopRecipesByAverageRating();

        assertTrue(recipes.size() > 1);

        verify(recipeRepository, times(1)).findTop50ByOrderByAverageRating();
        verifyNoMoreInteractions(recipeRepository);
    }

    private List<Recipe> createTestRecipesList() {
        List<Recipe> recipes = new ArrayList<>();

        for(Long i = 0L; i < 10L; i++)
            recipes.add(createTestRecipe(i));

        return recipes;
    }

    @Test
    public void create() {
        given(recipeRepository.save(any())).willReturn(createTestRecipe(1L));
        given(categoryRepository.findAllById(any())).willReturn(createTestCategories());
        given(ingredientRepository.findById(any())).willReturn(Optional.of(new Ingredient(1L, "test ingr", true)));
        given(ingredientAmountPerRecipeRepository.save(any())).willAnswer(a -> a.getArgument(0));

        Recipe recipe = recipeService.create(createTestRecipe(1L));

        assertThat(recipe.getId(), is(1L));
        assertThat(recipe.getName(), is("Test Recipe"));

        verify(recipeRepository, times(1)).save(any());
        verifyNoMoreInteractions(recipeRepository);
    }

    @Test
    @WithMockUser(username="test",roles={"USER","ADMIN"})
    public void update() {
        given(recipeRepository.findById(any())).willReturn(Optional.of(createTestRecipe(1L)));
        given(categoryRepository.findAllById(any())).willReturn(createTestCategories());
        given(ingredientRepository.findById(any())).willReturn(Optional.of(new Ingredient(1L, "test ingr", true)));
        given(ingredientAmountPerRecipeRepository.save(any())).willAnswer(a -> a.getArgument(0));
        given(recipeRepository.save(any())).willAnswer(a -> a.getArgument(0));

        Recipe testRecipe = createTestRecipe(1L);
        testRecipe.setName("New Test Name");
        Recipe recipe = recipeService.update(testRecipe);

        assertThat(recipe.getId(), is(1L));
        assertThat(recipe.getName(), is("New Test Name"));
        assertThat(recipe.getImage(), is("imgTEST.jpg"));


        verify(recipeRepository, times(2)).findById(any());
        verify(recipeRepository, times(1)).save(any());
        verifyNoMoreInteractions(recipeRepository);
    }


    @Test
    public void getAll() {
        given(recipeRepository.findAll()).willReturn(createTestRecipesList());

        List<Recipe> recipes = recipeService.getAll();

        assertTrue(recipes.size() > 1);

        verify(recipeRepository, times(1)).findAll();
        verifyNoMoreInteractions(recipeRepository);
    }
}