package xd.netteam.Eat.Less.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.*;
import xd.netteam.Eat.Less.util.results.Result;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTests {
    @Test
    public void hasExcludedReturnsTrueIfUserExcludedIngredient(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();

        // when
        user.exclude(ingredient);

        // then
        assertThat(user.hasExcluded(ingredient), is(true));
    }

    @Test
    public void hasExcludedReturnsFalseIfUserDidNotExcludeIngredient(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();

        // then
        assertThat(user.hasExcluded(ingredient), is(false));
    }

    @Test
    public void excludeAddsIngredientToUsersExcludedList(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();

        // when
        user.exclude(ingredient);

        // then
        assertTrue(user.getExcludedIngredients().size() > 0);
    }

    @Test
    public void excludingIngredientTwiceResultsInError(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();
        user.exclude(ingredient);

        // when
        Result result = user.exclude(ingredient);

        // then
        assertEquals(false, result.succeed());
    }

    @Test
    public void unexcludingIngredientRemovesItFromUsersExcludedList(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();
        user.exclude(ingredient);

        // when
        user.unexclude(ingredient);

        // then
        assertEquals(0, user.getExcludedIngredients().size());
    }

    @Test
    public void unexcludingIngredientThatWasNotExcludedResultsInError(){
        // given
        User user = new User();
        Ingredient ingredient = new Ingredient();

        // when
        Result result = user.unexclude(ingredient);

        // then
        assertThat(result.succeed(), is(false));
    }

    @Test
    public void likesRecipeReturnsTrueIfUserAddedItToHisFavouritesList(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();

        // when
        user.addToFavourites(recipe);

        // then
        assertTrue(user.likesRecipe(recipe));
    }

    @Test
    public void likesRecipeReturnsFalseIfUserDidNotAddItToHisFavouritesList(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();

        // then
        assertTrue(!user.likesRecipe(recipe));
    }

    @Test
    public void addToFavouritesAddsRecipeToUsersFavourites(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();

        // when
        user.addToFavourites(recipe);

        // then
        assertTrue(user.getFavouriteRecipes().size() > 0);
    }

    @Test
    public void addToFavouritesTwiceResultsInError(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();
        user.addToFavourites(recipe);

        // when
        Result result = user.addToFavourites(recipe);

        // then
        assertThat(result.succeed(), is(false));
    }

    @Test
    public void removeFromFavouritesRemovesRecipeFromUsersFavourites(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();
        user.addToFavourites(recipe);

        // when
        user.removeFromFavourites(recipe);

        // then
        assertEquals(0, user.getFavouriteRecipes().size());
    }

    @Test
    public void removeFromFavoritesResultsInErrorIfRecipeWasNotFavourite(){
        // given
        User user = new User();
        Recipe recipe = new Recipe();

        // when
        Result result = user.removeFromFavourites(recipe);

        // then
        assertThat(result.succeed(), is(false));
    }
}
