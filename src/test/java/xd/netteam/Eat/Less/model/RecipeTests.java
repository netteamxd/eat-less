package xd.netteam.Eat.Less.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@RunWith(SpringRunner.class)
public class RecipeTests {
    @Test
    public void hasCategoryReturnsTrueIfRecipeContainsCategory(){
        // given
        Recipe recipe = new Recipe();
        Category category = new Category();

        // when
        recipe.addCategory(category);

        // then
        assertThat(recipe.hasCategory(category), is(true));
    }

    @Test
    public void hasCategoryReturnsFalseIfRecipeDoesNotContainCategory(){
        // given
        Recipe recipe = new Recipe();
        Category category = new Category();

        // then
        assertThat(recipe.hasCategory(category), is(false));
    }

    @Test
    public void addCategoryAddsCategoryToRecipe(){
        // given
        Recipe recipe = new Recipe();
        Category category = new Category();

        // when
        recipe.addCategory(category);

        // then
        assertThat(recipe.hasCategory(category), is(true));
    }

    @Test
    public void removeCategoryRemovesCategoryFromRecipe(){
        // given
        Recipe recipe = new Recipe();
        Category category = new Category();
        recipe.addCategory(category);

        // when
        recipe.removeCategory(category);

        // then
        assertThat(recipe.hasCategory(category), is(false));
    }

    @Test
    public void hasReviewAuthoredByReturnsTrueWhenUserReviewedRecipe(){
        // given
        Recipe recipe = new Recipe();
        User user = new User();

        // when
        Review review = new Review();
        review.setAuthor(user);
        review.setRating(2);
        review.setRecipe(recipe);
        recipe.addReview(review);

        // then
        assertThat(recipe.hasReviewAuthoredBy(user), is(true));
    }

    @Test
    public void hasReviewAuthoredByReturnsFalseWhenUserDidNotReviewRecipe(){
        // given
        Recipe recipe = new Recipe();
        User user = new User();

        // then
        assertThat(recipe.hasReviewAuthoredBy(user), is(false));
    }

    @Test
    public void addReviewAddsReviewToRecipe(){
        // given
        Recipe recipe = new Recipe();
        User user = new User();

        // when
        Review review = new Review();
        review.setAuthor(user);
        review.setRating(2);
        review.setRecipe(recipe);
        recipe.addReview(review);

        // then
        assertTrue(recipe.getReviews().size() > 0);
    }

    @Test
    public void removeReviewRemovesReviewFromRecipe(){
        // given
        Recipe recipe = new Recipe();
        User user = new User();
        Review review = new Review();
        review.setAuthor(user);
        review.setRating(2);
        review.setRecipe(recipe);
        recipe.addReview(review);

        // when
        recipe.removeReview(review);

        // then
        assertEquals(0, recipe.getReviews().size());
    }

    @Test
    public void hasIngredientReturnsTrueIfRecipeContainsIngredient(){
        // given
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();

        // when
        recipe.addIngredient(new IngredientAmountPerRecipe(ingredient, "1 piece"));

        // then
        assertThat(recipe.hasIngredient(ingredient), is(true));
    }


    @Test
    public void hasIngredientReturnsFalseIfRecipeDoesNotContainIngredient(){
        // given
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();

        // then
        assertThat(recipe.hasIngredient(ingredient), is(false));
    }

    @Test
    public void recipeIsConsideredVeganIfItDoesNotContainAnimalProducts(){
        // given
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();
        ingredient.setAnimalProduct(false);

        // when
        recipe.addIngredient(new IngredientAmountPerRecipe(ingredient, "1 piece"));

        // then
        assertThat(recipe.isVegan(), is(true));
    }

    @Test
    public void recipeIsNotVeganIfItContainsAnimalProducts(){

        // given
        Recipe recipe = new Recipe();
        Ingredient ingredient = new Ingredient();
        ingredient.setAnimalProduct(true);

        // when
        recipe.addIngredient(new IngredientAmountPerRecipe(ingredient, "1 piece"));

        // then
        assertThat(recipe.isVegan(), is(false));
    }
}
