package xd.netteam.Eat.Less.integrational;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import xd.netteam.Eat.Less.model.entity.Category;
import xd.netteam.Eat.Less.model.entity.Ingredient;
import xd.netteam.Eat.Less.model.entity.IngredientAmountPerRecipe;
import xd.netteam.Eat.Less.model.entity.Recipe;
import xd.netteam.Eat.Less.repository.IngredientAmountPerRecipeRepository;
import xd.netteam.Eat.Less.service.CategoryService;
import xd.netteam.Eat.Less.service.IngredientService;
import xd.netteam.Eat.Less.service.RecipeService;
import xd.netteam.Eat.Less.util.search.SearchSpecificationBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
@WithMockUser(username="test",roles={"USER","ADMIN"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SearchRecipeTest {

    @Autowired
    RecipeService recipeService;

    @Autowired
    IngredientService ingredientService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    IngredientAmountPerRecipeRepository ingredientAmountPerRecipeRepository;

    private static boolean prepared = false;

    @Before
    public void prepare(){
        if(prepared) return;
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ArrayList<Category> categories = new ArrayList<>();
        ArrayList<Recipe> recipies = new ArrayList<>();

        //adding to arrays so that objects IDs will be equal to indexes
        ingredients.add(null);
        categories.add(null);
        recipies.add(null);

        for(int i = 0; i< 10; i++){
            ingredients.add(ingredientService.create(IngredientGenerator.get()));
        }
        for(int i = 0; i< 5; i++){
            categories.add(categoryService.create(CategoryGenerator.get()));
        }

        RecipeGenerator.prepareIngredients(ingredients.get(1), ingredients.get(2), ingredients.get(3));
        RecipeGenerator.prepareCategories(categories.get(1));
        recipies.add(recipeService.create(RecipeGenerator.get(400)));

        RecipeGenerator.prepareIngredients(ingredients.get(6), ingredients.get(7), ingredients.get(8));
        RecipeGenerator.prepareCategories(categories.get(1),categories.get(2));
        recipies.add(recipeService.create(RecipeGenerator.get(500)));

        RecipeGenerator.prepareIngredients(ingredients.get(1), ingredients.get(7), ingredients.get(9));
        RecipeGenerator.prepareCategories(categories.get(2));
        recipies.add(recipeService.create(RecipeGenerator.get(300)));

        RecipeGenerator.prepareIngredients(ingredients.get(1), ingredients.get(2), ingredients.get(3), ingredients.get(4));
        RecipeGenerator.prepareCategories(categories.get(3));
        recipies.add(recipeService.create(RecipeGenerator.get(450)));

        categories.get(5).setParentCategory(categories.get(4));
        categoryService.update(categories.get(5));

        RecipeGenerator.prepareIngredients(ingredients.get(1));
        RecipeGenerator.prepareCategories(categories.get(5));
        recipies.add(recipeService.create(RecipeGenerator.get(600)));

        prepared = true;
    }

    //delete data after all tests
    @Test
    public void zzzDeleteData(){
        recipeService.deleteAll();
        ingredientAmountPerRecipeRepository.deleteAll();
        ingredientService.deleteAll();
        categoryService.deleteAll();
    }

    @Test
    public void testShouldFindTwoRecipesByCaloriesNo(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.hasAtLeastCalories(350L);
        specificationBuilder.hasNoMoreCaloriesThan(450L);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(1L)));
        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(4L)));
    }

    @Test
    public void testShouldFindRecipeId3ByIngredients(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.containsIngredient(1L);
        specificationBuilder.containsIngredient(7L);
        specificationBuilder.containsIngredient(9L);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertThat(recipes.get(0), is(notNullValue()));
        assertThat(recipes.get(0).getId(), is(3L));
    }

    @Test
    public void testShouldFind3RecipesByCategories(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.inCategories(Arrays.asList(1L, 2L), categoryService);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(1L)));
        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(2L)));
        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(3L)));
    }

    @Test
    public void testShouldFindRecipeByName(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.nameContains("1");
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(1L)));
    }

    @Test
    public void testShouldFind2RecipesWithoutIngredient(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.doesNotContainIngredient(4L);
        specificationBuilder.doesNotContainIngredient(9L);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(1L)));
        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(2L)));
    }

    @Test
    public void testShouldFindRecipeByNestedCategory(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.inCategories(Arrays.asList(4L),categoryService);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(5L)));
    }

    @Test
    public void testShouldFindOneRecipe(){
        SearchSpecificationBuilder specificationBuilder = new SearchSpecificationBuilder();
        specificationBuilder.containsIngredient(2L);
        specificationBuilder.doesNotContainIngredient(7L);
        specificationBuilder.inCategories(Arrays.asList(2L, 3L), categoryService);
        specificationBuilder.nameContains("cip");
        specificationBuilder.hasNoMoreCaloriesThan(475L);
        specificationBuilder.hasAtLeastCalories(350L);
        List<Recipe> recipes = recipeService.search(specificationBuilder.build());

        assertTrue(recipes.stream().anyMatch(recipe -> recipe.getId().equals(4L)));
    }

    public static class IngredientGenerator{
        private static int count = 0;

        static Ingredient get(){
            Ingredient ingredient = new Ingredient();
            count++;
            ingredient.setAnimalProduct(count %2==0);
            ingredient.setName("Ingredient "+ count +(ingredient.isAnimalProduct()?"A":""));
            return ingredient;
        }
    }

    public static class CategoryGenerator{
        private static int count = 0;

        static Category get(){
            Category category = new Category();
            count++;
            category.setName("Category "+ count);
            return category;
        }
    }

    public static class RecipeGenerator{
        private static int count = 0;

        private static List<IngredientAmountPerRecipe> ingredients = new ArrayList<>();
        private static List<Category> categories = new ArrayList<>();

        static void prepareIngredients(Ingredient... ingredients){
            for(Ingredient i : ingredients){
                RecipeGenerator.ingredients.add(
                        new IngredientAmountPerRecipe(i,"amount "+i.getId()+" "+count));
            }
        }

        static void prepareCategories(Category... categories){
            Collections.addAll(RecipeGenerator.categories, categories);
        }

        static Recipe get(int totalCalories){
            Recipe recipe = new Recipe();
            count++;
            recipe.setName("Recipe "+ count);
            recipe.setContent("Description "+count+"\n" +
                    "1. Prepare ingredients\n" +
                    "2. Throw them into the bowl\n" +
                    "3. Mix" +
                    "4. Expect explosion");
            categories.forEach(recipe::addCategory);
            ingredients.forEach(recipe::addIngredient);
            recipe.setImage("imgTEST.jpg");
            recipe.setPortions(ingredients.size());
            recipe.setTotalCalories(totalCalories);
            recipe.setPreparationTime(30);
            ingredients = new ArrayList<>();
            categories = new ArrayList<>();
            return recipe;
        }
    }

}
